﻿namespace Agent_config
{
    partial class Counters_window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.text_box_threshold = new System.Windows.Forms.TextBox();
            this.button_accept_counter_config = new System.Windows.Forms.Button();
            this.list_box_template_counters = new System.Windows.Forms.ListBox();
            this.label_threshold = new System.Windows.Forms.Label();
            this.label_type_of_threshold = new System.Windows.Forms.Label();
            this.text_box_type_of_threshold = new System.Windows.Forms.TextBox();
            this.checked_list_editable_parameters_add = new System.Windows.Forms.CheckedListBox();
            this.text_box_threshold_editable = new System.Windows.Forms.TextBox();
            this.label_threshold_editable = new System.Windows.Forms.Label();
            this.label_type_of_threshold_editable = new System.Windows.Forms.Label();
            this.text_box_type_of_threshold_editable = new System.Windows.Forms.TextBox();
            this.button_add_editable_parameters = new System.Windows.Forms.Button();
            this.checked_list_editable_parameters_delete = new System.Windows.Forms.CheckedListBox();
            this.button_delete_editable_parameters = new System.Windows.Forms.Button();
            this.group_box_template_parameters = new System.Windows.Forms.GroupBox();
            this.text_box_number_of_values_to_exceed_threshold = new System.Windows.Forms.TextBox();
            this.label_number_of_values_to_exceed_threshold = new System.Windows.Forms.Label();
            this.combo_box_instance_name_template = new System.Windows.Forms.ComboBox();
            this.label_instance_name_template = new System.Windows.Forms.Label();
            this.groupBox_editable_parameters = new System.Windows.Forms.GroupBox();
            this.combo_box_instance_name_editable = new System.Windows.Forms.ComboBox();
            this.label_instance_name_editable = new System.Windows.Forms.Label();
            this.button_accept_editable_counter_config = new System.Windows.Forms.Button();
            this.list_box_editable_counters = new System.Windows.Forms.ListBox();
            this.group_box_manage_parameters = new System.Windows.Forms.GroupBox();
            this.combo_box_add_editable_parameters = new System.Windows.Forms.ComboBox();
            this.text_box_number_of_values_to_exceed_threshold_editable = new System.Windows.Forms.TextBox();
            this.label_number_of_values_to_exceed_threshold_editable = new System.Windows.Forms.Label();
            this.group_box_template_parameters.SuspendLayout();
            this.groupBox_editable_parameters.SuspendLayout();
            this.group_box_manage_parameters.SuspendLayout();
            this.SuspendLayout();
            // 
            // text_box_threshold
            // 
            this.text_box_threshold.Location = new System.Drawing.Point(316, 16);
            this.text_box_threshold.Name = "text_box_threshold";
            this.text_box_threshold.Size = new System.Drawing.Size(100, 20);
            this.text_box_threshold.TabIndex = 3;
            // 
            // button_accept_counter_config
            // 
            this.button_accept_counter_config.Location = new System.Drawing.Point(218, 126);
            this.button_accept_counter_config.Name = "button_accept_counter_config";
            this.button_accept_counter_config.Size = new System.Drawing.Size(198, 23);
            this.button_accept_counter_config.TabIndex = 4;
            this.button_accept_counter_config.Text = "OK";
            this.button_accept_counter_config.UseVisualStyleBackColor = true;
            this.button_accept_counter_config.Click += new System.EventHandler(this.button_accept_counter_config_Click);
            // 
            // list_box_template_counters
            // 
            this.list_box_template_counters.FormattingEnabled = true;
            this.list_box_template_counters.Location = new System.Drawing.Point(6, 28);
            this.list_box_template_counters.Name = "list_box_template_counters";
            this.list_box_template_counters.Size = new System.Drawing.Size(196, 121);
            this.list_box_template_counters.TabIndex = 6;
            this.list_box_template_counters.SelectedIndexChanged += new System.EventHandler(this.list_box_template_counters_SelectedIndexChanged);
            // 
            // label_threshold
            // 
            this.label_threshold.AutoSize = true;
            this.label_threshold.Location = new System.Drawing.Point(221, 20);
            this.label_threshold.Name = "label_threshold";
            this.label_threshold.Size = new System.Drawing.Size(54, 13);
            this.label_threshold.TabIndex = 8;
            this.label_threshold.Text = "Threshold";
            // 
            // label_type_of_threshold
            // 
            this.label_type_of_threshold.AutoSize = true;
            this.label_type_of_threshold.Location = new System.Drawing.Point(221, 48);
            this.label_type_of_threshold.Name = "label_type_of_threshold";
            this.label_type_of_threshold.Size = new System.Drawing.Size(89, 13);
            this.label_type_of_threshold.TabIndex = 9;
            this.label_type_of_threshold.Text = "Type of threshold";
            // 
            // text_box_type_of_threshold
            // 
            this.text_box_type_of_threshold.Location = new System.Drawing.Point(316, 45);
            this.text_box_type_of_threshold.Name = "text_box_type_of_threshold";
            this.text_box_type_of_threshold.Size = new System.Drawing.Size(100, 20);
            this.text_box_type_of_threshold.TabIndex = 10;
            // 
            // checked_list_editable_parameters_add
            // 
            this.checked_list_editable_parameters_add.FormattingEnabled = true;
            this.checked_list_editable_parameters_add.Location = new System.Drawing.Point(15, 48);
            this.checked_list_editable_parameters_add.Name = "checked_list_editable_parameters_add";
            this.checked_list_editable_parameters_add.Size = new System.Drawing.Size(196, 94);
            this.checked_list_editable_parameters_add.TabIndex = 11;
            // 
            // text_box_threshold_editable
            // 
            this.text_box_threshold_editable.Location = new System.Drawing.Point(316, 13);
            this.text_box_threshold_editable.Name = "text_box_threshold_editable";
            this.text_box_threshold_editable.Size = new System.Drawing.Size(100, 20);
            this.text_box_threshold_editable.TabIndex = 12;
            // 
            // label_threshold_editable
            // 
            this.label_threshold_editable.AutoSize = true;
            this.label_threshold_editable.Location = new System.Drawing.Point(221, 16);
            this.label_threshold_editable.Name = "label_threshold_editable";
            this.label_threshold_editable.Size = new System.Drawing.Size(54, 13);
            this.label_threshold_editable.TabIndex = 13;
            this.label_threshold_editable.Text = "Threshold";
            // 
            // label_type_of_threshold_editable
            // 
            this.label_type_of_threshold_editable.AutoSize = true;
            this.label_type_of_threshold_editable.Location = new System.Drawing.Point(221, 46);
            this.label_type_of_threshold_editable.Name = "label_type_of_threshold_editable";
            this.label_type_of_threshold_editable.Size = new System.Drawing.Size(89, 13);
            this.label_type_of_threshold_editable.TabIndex = 14;
            this.label_type_of_threshold_editable.Text = "Type of threshold";
            // 
            // text_box_type_of_threshold_editable
            // 
            this.text_box_type_of_threshold_editable.Location = new System.Drawing.Point(316, 43);
            this.text_box_type_of_threshold_editable.Name = "text_box_type_of_threshold_editable";
            this.text_box_type_of_threshold_editable.Size = new System.Drawing.Size(100, 20);
            this.text_box_type_of_threshold_editable.TabIndex = 15;
            // 
            // button_add_editable_parameters
            // 
            this.button_add_editable_parameters.Location = new System.Drawing.Point(15, 148);
            this.button_add_editable_parameters.Name = "button_add_editable_parameters";
            this.button_add_editable_parameters.Size = new System.Drawing.Size(198, 23);
            this.button_add_editable_parameters.TabIndex = 16;
            this.button_add_editable_parameters.Text = "Add";
            this.button_add_editable_parameters.UseVisualStyleBackColor = true;
            this.button_add_editable_parameters.Click += new System.EventHandler(this.button_add_editable_parameters_Click);
            // 
            // checked_list_editable_parameters_delete
            // 
            this.checked_list_editable_parameters_delete.FormattingEnabled = true;
            this.checked_list_editable_parameters_delete.Location = new System.Drawing.Point(19, 194);
            this.checked_list_editable_parameters_delete.Name = "checked_list_editable_parameters_delete";
            this.checked_list_editable_parameters_delete.Size = new System.Drawing.Size(196, 94);
            this.checked_list_editable_parameters_delete.TabIndex = 17;
            // 
            // button_delete_editable_parameters
            // 
            this.button_delete_editable_parameters.Location = new System.Drawing.Point(19, 292);
            this.button_delete_editable_parameters.Name = "button_delete_editable_parameters";
            this.button_delete_editable_parameters.Size = new System.Drawing.Size(198, 23);
            this.button_delete_editable_parameters.TabIndex = 18;
            this.button_delete_editable_parameters.Text = "Delete";
            this.button_delete_editable_parameters.UseVisualStyleBackColor = true;
            this.button_delete_editable_parameters.Click += new System.EventHandler(this.button_delete_editable_parameters_Click);
            // 
            // group_box_template_parameters
            // 
            this.group_box_template_parameters.Controls.Add(this.text_box_number_of_values_to_exceed_threshold);
            this.group_box_template_parameters.Controls.Add(this.label_number_of_values_to_exceed_threshold);
            this.group_box_template_parameters.Controls.Add(this.combo_box_instance_name_template);
            this.group_box_template_parameters.Controls.Add(this.label_instance_name_template);
            this.group_box_template_parameters.Controls.Add(this.button_accept_counter_config);
            this.group_box_template_parameters.Controls.Add(this.list_box_template_counters);
            this.group_box_template_parameters.Controls.Add(this.label_threshold);
            this.group_box_template_parameters.Controls.Add(this.text_box_threshold);
            this.group_box_template_parameters.Controls.Add(this.label_type_of_threshold);
            this.group_box_template_parameters.Controls.Add(this.text_box_type_of_threshold);
            this.group_box_template_parameters.Location = new System.Drawing.Point(36, 37);
            this.group_box_template_parameters.Name = "group_box_template_parameters";
            this.group_box_template_parameters.Size = new System.Drawing.Size(429, 160);
            this.group_box_template_parameters.TabIndex = 19;
            this.group_box_template_parameters.TabStop = false;
            this.group_box_template_parameters.Text = "Template parameters";
            // 
            // text_box_number_of_values_to_exceed_threshold
            // 
            this.text_box_number_of_values_to_exceed_threshold.Location = new System.Drawing.Point(316, 98);
            this.text_box_number_of_values_to_exceed_threshold.Name = "text_box_number_of_values_to_exceed_threshold";
            this.text_box_number_of_values_to_exceed_threshold.Size = new System.Drawing.Size(100, 20);
            this.text_box_number_of_values_to_exceed_threshold.TabIndex = 25;
            // 
            // label_number_of_values_to_exceed_threshold
            // 
            this.label_number_of_values_to_exceed_threshold.AutoSize = true;
            this.label_number_of_values_to_exceed_threshold.Location = new System.Drawing.Point(221, 105);
            this.label_number_of_values_to_exceed_threshold.Name = "label_number_of_values_to_exceed_threshold";
            this.label_number_of_values_to_exceed_threshold.Size = new System.Drawing.Size(72, 13);
            this.label_number_of_values_to_exceed_threshold.TabIndex = 24;
            this.label_number_of_values_to_exceed_threshold.Text = "Alarm counter";
            // 
            // combo_box_instance_name_template
            // 
            this.combo_box_instance_name_template.FormattingEnabled = true;
            this.combo_box_instance_name_template.Location = new System.Drawing.Point(316, 71);
            this.combo_box_instance_name_template.Name = "combo_box_instance_name_template";
            this.combo_box_instance_name_template.Size = new System.Drawing.Size(100, 21);
            this.combo_box_instance_name_template.TabIndex = 23;
            // 
            // label_instance_name_template
            // 
            this.label_instance_name_template.AutoSize = true;
            this.label_instance_name_template.Location = new System.Drawing.Point(221, 77);
            this.label_instance_name_template.Name = "label_instance_name_template";
            this.label_instance_name_template.Size = new System.Drawing.Size(77, 13);
            this.label_instance_name_template.TabIndex = 12;
            this.label_instance_name_template.Text = "Instance name";
            // 
            // groupBox_editable_parameters
            // 
            this.groupBox_editable_parameters.Controls.Add(this.label_number_of_values_to_exceed_threshold_editable);
            this.groupBox_editable_parameters.Controls.Add(this.text_box_number_of_values_to_exceed_threshold_editable);
            this.groupBox_editable_parameters.Controls.Add(this.combo_box_instance_name_editable);
            this.groupBox_editable_parameters.Controls.Add(this.label_instance_name_editable);
            this.groupBox_editable_parameters.Controls.Add(this.button_accept_editable_counter_config);
            this.groupBox_editable_parameters.Controls.Add(this.list_box_editable_counters);
            this.groupBox_editable_parameters.Controls.Add(this.label_threshold_editable);
            this.groupBox_editable_parameters.Controls.Add(this.text_box_threshold_editable);
            this.groupBox_editable_parameters.Controls.Add(this.text_box_type_of_threshold_editable);
            this.groupBox_editable_parameters.Controls.Add(this.label_type_of_threshold_editable);
            this.groupBox_editable_parameters.Location = new System.Drawing.Point(36, 203);
            this.groupBox_editable_parameters.Name = "groupBox_editable_parameters";
            this.groupBox_editable_parameters.Size = new System.Drawing.Size(429, 162);
            this.groupBox_editable_parameters.TabIndex = 20;
            this.groupBox_editable_parameters.TabStop = false;
            this.groupBox_editable_parameters.Text = "Editable parameters";
            // 
            // combo_box_instance_name_editable
            // 
            this.combo_box_instance_name_editable.FormattingEnabled = true;
            this.combo_box_instance_name_editable.Location = new System.Drawing.Point(316, 72);
            this.combo_box_instance_name_editable.Name = "combo_box_instance_name_editable";
            this.combo_box_instance_name_editable.Size = new System.Drawing.Size(100, 21);
            this.combo_box_instance_name_editable.TabIndex = 22;
            // 
            // label_instance_name_editable
            // 
            this.label_instance_name_editable.AutoSize = true;
            this.label_instance_name_editable.Location = new System.Drawing.Point(221, 72);
            this.label_instance_name_editable.Name = "label_instance_name_editable";
            this.label_instance_name_editable.Size = new System.Drawing.Size(77, 13);
            this.label_instance_name_editable.TabIndex = 16;
            this.label_instance_name_editable.Text = "Instance name";
            // 
            // button_accept_editable_counter_config
            // 
            this.button_accept_editable_counter_config.Location = new System.Drawing.Point(218, 126);
            this.button_accept_editable_counter_config.Name = "button_accept_editable_counter_config";
            this.button_accept_editable_counter_config.Size = new System.Drawing.Size(198, 23);
            this.button_accept_editable_counter_config.TabIndex = 4;
            this.button_accept_editable_counter_config.Text = "OK";
            this.button_accept_editable_counter_config.UseVisualStyleBackColor = true;
            this.button_accept_editable_counter_config.Click += new System.EventHandler(this.button_accept_editable_counter_config_Click);
            // 
            // list_box_editable_counters
            // 
            this.list_box_editable_counters.FormattingEnabled = true;
            this.list_box_editable_counters.Location = new System.Drawing.Point(6, 28);
            this.list_box_editable_counters.Name = "list_box_editable_counters";
            this.list_box_editable_counters.Size = new System.Drawing.Size(196, 121);
            this.list_box_editable_counters.TabIndex = 6;
            this.list_box_editable_counters.SelectedIndexChanged += new System.EventHandler(this.list_box_editable_counters_SelectedIndexChanged);
            // 
            // group_box_manage_parameters
            // 
            this.group_box_manage_parameters.Controls.Add(this.combo_box_add_editable_parameters);
            this.group_box_manage_parameters.Controls.Add(this.checked_list_editable_parameters_add);
            this.group_box_manage_parameters.Controls.Add(this.button_add_editable_parameters);
            this.group_box_manage_parameters.Controls.Add(this.checked_list_editable_parameters_delete);
            this.group_box_manage_parameters.Controls.Add(this.button_delete_editable_parameters);
            this.group_box_manage_parameters.Location = new System.Drawing.Point(482, 37);
            this.group_box_manage_parameters.Name = "group_box_manage_parameters";
            this.group_box_manage_parameters.Size = new System.Drawing.Size(234, 328);
            this.group_box_manage_parameters.TabIndex = 21;
            this.group_box_manage_parameters.TabStop = false;
            this.group_box_manage_parameters.Text = "Parameters management";
            // 
            // combo_box_add_editable_parameters
            // 
            this.combo_box_add_editable_parameters.FormattingEnabled = true;
            this.combo_box_add_editable_parameters.Location = new System.Drawing.Point(15, 20);
            this.combo_box_add_editable_parameters.Name = "combo_box_add_editable_parameters";
            this.combo_box_add_editable_parameters.Size = new System.Drawing.Size(196, 21);
            this.combo_box_add_editable_parameters.TabIndex = 19;
            this.combo_box_add_editable_parameters.SelectedIndexChanged += new System.EventHandler(this.combo_box_add_editable_parameters_SelectedIndexChanged);
            // 
            // text_box_number_of_values_to_exceed_threshold_editable
            // 
            this.text_box_number_of_values_to_exceed_threshold_editable.Location = new System.Drawing.Point(316, 99);
            this.text_box_number_of_values_to_exceed_threshold_editable.Name = "text_box_number_of_values_to_exceed_threshold_editable";
            this.text_box_number_of_values_to_exceed_threshold_editable.Size = new System.Drawing.Size(100, 20);
            this.text_box_number_of_values_to_exceed_threshold_editable.TabIndex = 23;
            // 
            // label_number_of_values_to_exceed_threshold_editable
            // 
            this.label_number_of_values_to_exceed_threshold_editable.AutoSize = true;
            this.label_number_of_values_to_exceed_threshold_editable.Location = new System.Drawing.Point(221, 99);
            this.label_number_of_values_to_exceed_threshold_editable.Name = "label_number_of_values_to_exceed_threshold_editable";
            this.label_number_of_values_to_exceed_threshold_editable.Size = new System.Drawing.Size(77, 13);
            this.label_number_of_values_to_exceed_threshold_editable.TabIndex = 24;
            this.label_number_of_values_to_exceed_threshold_editable.Text = "Alarm counter";
            // 
            // Counters_window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 401);
            this.Controls.Add(this.group_box_manage_parameters);
            this.Controls.Add(this.groupBox_editable_parameters);
            this.Controls.Add(this.group_box_template_parameters);
            this.Name = "Counters_window";
            this.Text = "Performance counters configuration";
            this.group_box_template_parameters.ResumeLayout(false);
            this.group_box_template_parameters.PerformLayout();
            this.groupBox_editable_parameters.ResumeLayout(false);
            this.groupBox_editable_parameters.PerformLayout();
            this.group_box_manage_parameters.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox list_box_template_counters;
        private System.Windows.Forms.Label label_threshold;
        private System.Windows.Forms.Label label_type_of_threshold;
        private System.Windows.Forms.TextBox text_box_type_of_threshold;
        private System.Windows.Forms.TextBox text_box_threshold;
        private System.Windows.Forms.Button button_accept_counter_config;
        private System.Windows.Forms.CheckedListBox checked_list_editable_parameters_add;
        private System.Windows.Forms.TextBox text_box_threshold_editable;
        private System.Windows.Forms.Label label_threshold_editable;
        private System.Windows.Forms.Label label_type_of_threshold_editable;
        private System.Windows.Forms.TextBox text_box_type_of_threshold_editable;
        private System.Windows.Forms.Button button_add_editable_parameters;
        private System.Windows.Forms.CheckedListBox checked_list_editable_parameters_delete;
        private System.Windows.Forms.Button button_delete_editable_parameters;
        private System.Windows.Forms.GroupBox group_box_template_parameters;
        private System.Windows.Forms.GroupBox groupBox_editable_parameters;
        private System.Windows.Forms.Button button_accept_editable_counter_config;
        private System.Windows.Forms.ListBox list_box_editable_counters;
        private System.Windows.Forms.GroupBox group_box_manage_parameters;
        private System.Windows.Forms.Label label_instance_name_editable;
        private System.Windows.Forms.Label label_instance_name_template;
        private System.Windows.Forms.ComboBox combo_box_add_editable_parameters;
        private System.Windows.Forms.ComboBox combo_box_instance_name_editable;
        private System.Windows.Forms.ComboBox combo_box_instance_name_template;
        private System.Windows.Forms.TextBox text_box_number_of_values_to_exceed_threshold;
        private System.Windows.Forms.Label label_number_of_values_to_exceed_threshold;
        private System.Windows.Forms.Label label_number_of_values_to_exceed_threshold_editable;
        private System.Windows.Forms.TextBox text_box_number_of_values_to_exceed_threshold_editable;
    }
}