﻿using System;
using AgentConfigurationSerializerLibrary;
using System.Windows.Forms;

namespace Agent_config
{
    public partial class Main_window : Form
    {
        Counters_window counters_window;
        Agent_window agent_window;
        Host_window host_window;

        AgentSerializer agent_serializer;
        Configuration config;

        string connection_string;
        string config_file_path;

        public Main_window()
        {
            InitializeComponent();
            //config_file_path = @"config_file.xml";
            config_file_path = @"B:\ZabbixAgentAsService\config_file.xml";

            config = new Configuration();
            agent_serializer = new AgentSerializer(config_file_path);

            config = agent_serializer.read_from_config_file();
            connection_string = "SERVER=" + config.server + "; PORT = 3306 ;" + "DATABASE=" + config.database + ";" + "UID=" + config.uid + ";" + "PASSWORD=" + config.password + ";";
        }

        private void button_main_agent_Click(object sender, EventArgs e)
        {
            agent_window = new Agent_window(config_file_path, config, agent_serializer);
            agent_window.Show();
        }

        private void button_main_counters_Click(object sender, EventArgs e)
        {
            counters_window = new Counters_window(connection_string, config, agent_serializer);
            counters_window.Show();
        }

        private void button_main_host_Click(object sender, EventArgs e)
        {
            host_window = new Host_window(connection_string, config, agent_serializer);
            host_window.Show();
        }
    }
}
