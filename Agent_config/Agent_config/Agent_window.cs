﻿using System;
using System.Windows.Forms;
using AgentConfigurationSerializerLibrary;

namespace Agent_config
{
    public partial class Agent_window : Form
    {
        string config_file_path;
        Configuration config;
        AgentSerializer agent_serializer;

        public Agent_window(string _config_file_path, Configuration _config, AgentSerializer _agent_serializer)
        {
            InitializeComponent();
            config_file_path = _config_file_path;
            config = _config;
            agent_serializer = _agent_serializer;
        }

        private void menu_zabbix_server_Click(object sender, EventArgs e)
        {
            panel_zabbix_server.Visible = true;
            panel_agent_configuration.Visible = false;
            panel_perfmon_configuration.Visible = false;
        }

        private void menu_agent_configuration_Click(object sender, EventArgs e)
        {
            panel_zabbix_server.Visible = false;
            panel_agent_configuration.Visible = true;
            panel_perfmon_configuration.Visible = false;
        }

        private void menu_perfmon_configuration_Click(object sender, EventArgs e)
        {
            panel_zabbix_server.Visible = false;
            panel_agent_configuration.Visible = false;
            panel_perfmon_configuration.Visible = true;
        }

        private void button_accept_agent_config_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(text_box_server_ip.Text))
                config.server = text_box_server_ip.Text;
            if (!String.IsNullOrEmpty(text_box_database_name.Text))
                config.database = text_box_database_name.Text;
            if (!String.IsNullOrEmpty(text_box_uid.Text))
                config.uid = text_box_uid.Text;
            if (!String.IsNullOrEmpty(text_box_password.Text))
                config.password = text_box_password.Text;
            if (!String.IsNullOrEmpty(text_box_log_path.Text))
                config.log_file_path = text_box_log_path.Text;
            if (!String.IsNullOrEmpty(text_box_data_timer.Text))
                config.data_module_timer_interval = Int32.Parse(text_box_data_timer.Text); 
            if (!String.IsNullOrEmpty(text_box_process_timer.Text))
                config.process_module_timer_interval = Int32.Parse(text_box_process_timer.Text);
            if (!String.IsNullOrEmpty(text_box_analyzer_timer.Text))
                config.analyzer_module_timer_interval = Int32.Parse(text_box_analyzer_timer.Text);
            if (!String.IsNullOrEmpty(text_box_sender_timer.Text))
                config.sender_module_timer_interval = Int32.Parse(text_box_sender_timer.Text);
            if (!String.IsNullOrEmpty(text_box_perfmon_time.Text))
                config.perfmon_time = Int32.Parse(text_box_perfmon_time.Text);
            if (!String.IsNullOrEmpty(text_box_perfmon_path.Text))
                config.perfmon_path_reports = text_box_perfmon_path.Text;
            if (!String.IsNullOrEmpty(text_box_perfmon_module_name.Text))
                config.perfmon_module_name = text_box_perfmon_module_name.Text;

            agent_serializer.write_to_config_file(config);

            string message = "Agent configuration updated.";
            MessageBox.Show(message);
        }
    }
}
