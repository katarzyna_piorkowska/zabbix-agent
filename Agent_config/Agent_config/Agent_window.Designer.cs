﻿namespace Agent_config
{
    partial class Agent_window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.group_box_zabbix_server = new System.Windows.Forms.GroupBox();
            this.text_box_password = new System.Windows.Forms.TextBox();
            this.label_password = new System.Windows.Forms.Label();
            this.text_box_uid = new System.Windows.Forms.TextBox();
            this.label_uid = new System.Windows.Forms.Label();
            this.text_box_database_name = new System.Windows.Forms.TextBox();
            this.label_database_name = new System.Windows.Forms.Label();
            this.text_box_server_ip = new System.Windows.Forms.TextBox();
            this.label_server_ip = new System.Windows.Forms.Label();
            this.panel_zabbix_server = new System.Windows.Forms.Panel();
            this.menu_zabbix_server = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_agent_configuration = new System.Windows.Forms.ToolStripMenuItem();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menu_perfmon_configuration = new System.Windows.Forms.ToolStripMenuItem();
            this.panel_agent_configuration = new System.Windows.Forms.Panel();
            this.group_box_agent_config_timers = new System.Windows.Forms.GroupBox();
            this.text_box_sender_timer = new System.Windows.Forms.TextBox();
            this.label_sender_timer = new System.Windows.Forms.Label();
            this.text_box_analyzer_timer = new System.Windows.Forms.TextBox();
            this.label_analyzer_timer = new System.Windows.Forms.Label();
            this.text_box_process_timer = new System.Windows.Forms.TextBox();
            this.label_process_timer = new System.Windows.Forms.Label();
            this.label_data_timer = new System.Windows.Forms.Label();
            this.text_box_data_timer = new System.Windows.Forms.TextBox();
            this.group_box_agent_config_path = new System.Windows.Forms.GroupBox();
            this.label_log_path = new System.Windows.Forms.Label();
            this.text_box_log_path = new System.Windows.Forms.TextBox();
            this.button_accept_agent_config = new System.Windows.Forms.Button();
            this.panel_perfmon_configuration = new System.Windows.Forms.Panel();
            this.group_box_perfmon_config = new System.Windows.Forms.GroupBox();
            this.text_box_perfmon_path = new System.Windows.Forms.TextBox();
            this.label_perfmon_path = new System.Windows.Forms.Label();
            this.text_box_perfmon_module_name = new System.Windows.Forms.TextBox();
            this.label_perfmon_module_name = new System.Windows.Forms.Label();
            this.text_box_perfmon_time = new System.Windows.Forms.TextBox();
            this.label_perfmon_time = new System.Windows.Forms.Label();
            this.group_box_zabbix_server.SuspendLayout();
            this.panel_zabbix_server.SuspendLayout();
            this.menu.SuspendLayout();
            this.panel_agent_configuration.SuspendLayout();
            this.group_box_agent_config_timers.SuspendLayout();
            this.group_box_agent_config_path.SuspendLayout();
            this.panel_perfmon_configuration.SuspendLayout();
            this.group_box_perfmon_config.SuspendLayout();
            this.SuspendLayout();
            // 
            // group_box_zabbix_server
            // 
            this.group_box_zabbix_server.Controls.Add(this.text_box_password);
            this.group_box_zabbix_server.Controls.Add(this.label_password);
            this.group_box_zabbix_server.Controls.Add(this.text_box_uid);
            this.group_box_zabbix_server.Controls.Add(this.label_uid);
            this.group_box_zabbix_server.Controls.Add(this.text_box_database_name);
            this.group_box_zabbix_server.Controls.Add(this.label_database_name);
            this.group_box_zabbix_server.Controls.Add(this.text_box_server_ip);
            this.group_box_zabbix_server.Controls.Add(this.label_server_ip);
            this.group_box_zabbix_server.Location = new System.Drawing.Point(22, 14);
            this.group_box_zabbix_server.Name = "group_box_zabbix_server";
            this.group_box_zabbix_server.Size = new System.Drawing.Size(204, 142);
            this.group_box_zabbix_server.TabIndex = 0;
            this.group_box_zabbix_server.TabStop = false;
            this.group_box_zabbix_server.Text = "Zabbix server";
            // 
            // text_box_password
            // 
            this.text_box_password.Location = new System.Drawing.Point(94, 106);
            this.text_box_password.Name = "text_box_password";
            this.text_box_password.Size = new System.Drawing.Size(100, 20);
            this.text_box_password.TabIndex = 7;
            // 
            // label_password
            // 
            this.label_password.AutoSize = true;
            this.label_password.Location = new System.Drawing.Point(33, 106);
            this.label_password.Name = "label_password";
            this.label_password.Size = new System.Drawing.Size(53, 13);
            this.label_password.TabIndex = 6;
            this.label_password.Text = "Password";
            // 
            // text_box_uid
            // 
            this.text_box_uid.Location = new System.Drawing.Point(94, 80);
            this.text_box_uid.Name = "text_box_uid";
            this.text_box_uid.Size = new System.Drawing.Size(100, 20);
            this.text_box_uid.TabIndex = 5;
            // 
            // label_uid
            // 
            this.label_uid.AutoSize = true;
            this.label_uid.Location = new System.Drawing.Point(33, 80);
            this.label_uid.Name = "label_uid";
            this.label_uid.Size = new System.Drawing.Size(55, 13);
            this.label_uid.TabIndex = 4;
            this.label_uid.Text = "Username";
            // 
            // text_box_database_name
            // 
            this.text_box_database_name.Location = new System.Drawing.Point(94, 54);
            this.text_box_database_name.Name = "text_box_database_name";
            this.text_box_database_name.Size = new System.Drawing.Size(100, 20);
            this.text_box_database_name.TabIndex = 3;
            // 
            // label_database_name
            // 
            this.label_database_name.AutoSize = true;
            this.label_database_name.Location = new System.Drawing.Point(6, 54);
            this.label_database_name.Name = "label_database_name";
            this.label_database_name.Size = new System.Drawing.Size(82, 13);
            this.label_database_name.TabIndex = 2;
            this.label_database_name.Text = "Database name";
            // 
            // text_box_server_ip
            // 
            this.text_box_server_ip.Location = new System.Drawing.Point(94, 25);
            this.text_box_server_ip.Name = "text_box_server_ip";
            this.text_box_server_ip.Size = new System.Drawing.Size(100, 20);
            this.text_box_server_ip.TabIndex = 1;
            // 
            // label_server_ip
            // 
            this.label_server_ip.AutoSize = true;
            this.label_server_ip.Location = new System.Drawing.Point(37, 28);
            this.label_server_ip.Name = "label_server_ip";
            this.label_server_ip.Size = new System.Drawing.Size(51, 13);
            this.label_server_ip.TabIndex = 0;
            this.label_server_ip.Text = "Server IP";
            // 
            // panel_zabbix_server
            // 
            this.panel_zabbix_server.Controls.Add(this.group_box_zabbix_server);
            this.panel_zabbix_server.Location = new System.Drawing.Point(12, 27);
            this.panel_zabbix_server.Name = "panel_zabbix_server";
            this.panel_zabbix_server.Size = new System.Drawing.Size(438, 177);
            this.panel_zabbix_server.TabIndex = 1;
            // 
            // menu_zabbix_server
            // 
            this.menu_zabbix_server.Name = "menu_zabbix_server";
            this.menu_zabbix_server.Size = new System.Drawing.Size(88, 20);
            this.menu_zabbix_server.Text = "Zabbix server";
            this.menu_zabbix_server.Click += new System.EventHandler(this.menu_zabbix_server_Click);
            // 
            // menu_agent_configuration
            // 
            this.menu_agent_configuration.Name = "menu_agent_configuration";
            this.menu_agent_configuration.Size = new System.Drawing.Size(126, 20);
            this.menu_agent_configuration.Text = "Agent configuration";
            this.menu_agent_configuration.Click += new System.EventHandler(this.menu_agent_configuration_Click);
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_zabbix_server,
            this.menu_agent_configuration,
            this.menu_perfmon_configuration});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(493, 24);
            this.menu.TabIndex = 2;
            this.menu.Text = "menu";
            // 
            // menu_perfmon_configuration
            // 
            this.menu_perfmon_configuration.Name = "menu_perfmon_configuration";
            this.menu_perfmon_configuration.Size = new System.Drawing.Size(208, 20);
            this.menu_perfmon_configuration.Text = "Performance monitor configuration";
            this.menu_perfmon_configuration.Click += new System.EventHandler(this.menu_perfmon_configuration_Click);
            // 
            // panel_agent_configuration
            // 
            this.panel_agent_configuration.Controls.Add(this.group_box_agent_config_timers);
            this.panel_agent_configuration.Controls.Add(this.group_box_agent_config_path);
            this.panel_agent_configuration.Location = new System.Drawing.Point(12, 26);
            this.panel_agent_configuration.Name = "panel_agent_configuration";
            this.panel_agent_configuration.Size = new System.Drawing.Size(420, 285);
            this.panel_agent_configuration.TabIndex = 3;
            // 
            // group_box_agent_config_timers
            // 
            this.group_box_agent_config_timers.Controls.Add(this.text_box_sender_timer);
            this.group_box_agent_config_timers.Controls.Add(this.label_sender_timer);
            this.group_box_agent_config_timers.Controls.Add(this.text_box_analyzer_timer);
            this.group_box_agent_config_timers.Controls.Add(this.label_analyzer_timer);
            this.group_box_agent_config_timers.Controls.Add(this.text_box_process_timer);
            this.group_box_agent_config_timers.Controls.Add(this.label_process_timer);
            this.group_box_agent_config_timers.Controls.Add(this.label_data_timer);
            this.group_box_agent_config_timers.Controls.Add(this.text_box_data_timer);
            this.group_box_agent_config_timers.Location = new System.Drawing.Point(22, 80);
            this.group_box_agent_config_timers.Name = "group_box_agent_config_timers";
            this.group_box_agent_config_timers.Size = new System.Drawing.Size(235, 152);
            this.group_box_agent_config_timers.TabIndex = 3;
            this.group_box_agent_config_timers.TabStop = false;
            this.group_box_agent_config_timers.Text = "Timer intervals";
            // 
            // text_box_sender_timer
            // 
            this.text_box_sender_timer.Location = new System.Drawing.Point(167, 115);
            this.text_box_sender_timer.Name = "text_box_sender_timer";
            this.text_box_sender_timer.Size = new System.Drawing.Size(53, 20);
            this.text_box_sender_timer.TabIndex = 7;
            // 
            // label_sender_timer
            // 
            this.label_sender_timer.AutoSize = true;
            this.label_sender_timer.Location = new System.Drawing.Point(15, 118);
            this.label_sender_timer.Name = "label_sender_timer";
            this.label_sender_timer.Size = new System.Drawing.Size(140, 13);
            this.label_sender_timer.TabIndex = 6;
            this.label_sender_timer.Text = "Sender module timer interval";
            // 
            // text_box_analyzer_timer
            // 
            this.text_box_analyzer_timer.Location = new System.Drawing.Point(167, 83);
            this.text_box_analyzer_timer.Name = "text_box_analyzer_timer";
            this.text_box_analyzer_timer.Size = new System.Drawing.Size(53, 20);
            this.text_box_analyzer_timer.TabIndex = 5;
            // 
            // label_analyzer_timer
            // 
            this.label_analyzer_timer.AutoSize = true;
            this.label_analyzer_timer.Location = new System.Drawing.Point(17, 86);
            this.label_analyzer_timer.Name = "label_analyzer_timer";
            this.label_analyzer_timer.Size = new System.Drawing.Size(146, 13);
            this.label_analyzer_timer.TabIndex = 4;
            this.label_analyzer_timer.Text = "Analyzer module timer interval";
            // 
            // text_box_process_timer
            // 
            this.text_box_process_timer.Location = new System.Drawing.Point(167, 51);
            this.text_box_process_timer.Name = "text_box_process_timer";
            this.text_box_process_timer.Size = new System.Drawing.Size(53, 20);
            this.text_box_process_timer.TabIndex = 3;
            // 
            // label_process_timer
            // 
            this.label_process_timer.AutoSize = true;
            this.label_process_timer.Location = new System.Drawing.Point(17, 54);
            this.label_process_timer.Name = "label_process_timer";
            this.label_process_timer.Size = new System.Drawing.Size(144, 13);
            this.label_process_timer.TabIndex = 2;
            this.label_process_timer.Text = "Process module timer interval";
            // 
            // label_data_timer
            // 
            this.label_data_timer.AutoSize = true;
            this.label_data_timer.Location = new System.Drawing.Point(32, 22);
            this.label_data_timer.Name = "label_data_timer";
            this.label_data_timer.Size = new System.Drawing.Size(129, 13);
            this.label_data_timer.TabIndex = 0;
            this.label_data_timer.Text = "Data module timer interval";
            // 
            // text_box_data_timer
            // 
            this.text_box_data_timer.Location = new System.Drawing.Point(167, 19);
            this.text_box_data_timer.Name = "text_box_data_timer";
            this.text_box_data_timer.Size = new System.Drawing.Size(53, 20);
            this.text_box_data_timer.TabIndex = 1;
            // 
            // group_box_agent_config_path
            // 
            this.group_box_agent_config_path.Controls.Add(this.label_log_path);
            this.group_box_agent_config_path.Controls.Add(this.text_box_log_path);
            this.group_box_agent_config_path.Location = new System.Drawing.Point(20, 14);
            this.group_box_agent_config_path.Name = "group_box_agent_config_path";
            this.group_box_agent_config_path.Size = new System.Drawing.Size(237, 50);
            this.group_box_agent_config_path.TabIndex = 2;
            this.group_box_agent_config_path.TabStop = false;
            this.group_box_agent_config_path.Text = "Paths";
            // 
            // label_log_path
            // 
            this.label_log_path.AutoSize = true;
            this.label_log_path.Location = new System.Drawing.Point(45, 22);
            this.label_log_path.Name = "label_log_path";
            this.label_log_path.Size = new System.Drawing.Size(65, 13);
            this.label_log_path.TabIndex = 0;
            this.label_log_path.Text = "Log file path";
            // 
            // text_box_log_path
            // 
            this.text_box_log_path.Location = new System.Drawing.Point(122, 19);
            this.text_box_log_path.Name = "text_box_log_path";
            this.text_box_log_path.Size = new System.Drawing.Size(100, 20);
            this.text_box_log_path.TabIndex = 1;
            // 
            // button_accept_agent_config
            // 
            this.button_accept_agent_config.Location = new System.Drawing.Point(12, 318);
            this.button_accept_agent_config.Name = "button_accept_agent_config";
            this.button_accept_agent_config.Size = new System.Drawing.Size(123, 23);
            this.button_accept_agent_config.TabIndex = 4;
            this.button_accept_agent_config.Text = "OK";
            this.button_accept_agent_config.UseVisualStyleBackColor = true;
            this.button_accept_agent_config.Click += new System.EventHandler(this.button_accept_agent_config_Click);
            // 
            // panel_perfmon_configuration
            // 
            this.panel_perfmon_configuration.Controls.Add(this.group_box_perfmon_config);
            this.panel_perfmon_configuration.Location = new System.Drawing.Point(12, 26);
            this.panel_perfmon_configuration.Name = "panel_perfmon_configuration";
            this.panel_perfmon_configuration.Size = new System.Drawing.Size(398, 267);
            this.panel_perfmon_configuration.TabIndex = 5;
            // 
            // group_box_perfmon_config
            // 
            this.group_box_perfmon_config.Controls.Add(this.text_box_perfmon_path);
            this.group_box_perfmon_config.Controls.Add(this.label_perfmon_path);
            this.group_box_perfmon_config.Controls.Add(this.text_box_perfmon_module_name);
            this.group_box_perfmon_config.Controls.Add(this.label_perfmon_module_name);
            this.group_box_perfmon_config.Controls.Add(this.text_box_perfmon_time);
            this.group_box_perfmon_config.Controls.Add(this.label_perfmon_time);
            this.group_box_perfmon_config.Location = new System.Drawing.Point(22, 15);
            this.group_box_perfmon_config.Name = "group_box_perfmon_config";
            this.group_box_perfmon_config.Size = new System.Drawing.Size(192, 124);
            this.group_box_perfmon_config.TabIndex = 0;
            this.group_box_perfmon_config.TabStop = false;
            this.group_box_perfmon_config.Text = "Performance monitor";
            // 
            // text_box_perfmon_path
            // 
            this.text_box_perfmon_path.Location = new System.Drawing.Point(85, 93);
            this.text_box_perfmon_path.Name = "text_box_perfmon_path";
            this.text_box_perfmon_path.Size = new System.Drawing.Size(100, 20);
            this.text_box_perfmon_path.TabIndex = 5;
            // 
            // label_perfmon_path
            // 
            this.label_perfmon_path.AutoSize = true;
            this.label_perfmon_path.Location = new System.Drawing.Point(15, 96);
            this.label_perfmon_path.Name = "label_perfmon_path";
            this.label_perfmon_path.Size = new System.Drawing.Size(63, 13);
            this.label_perfmon_path.TabIndex = 4;
            this.label_perfmon_path.Text = "Report path";
            // 
            // text_box_perfmon_module_name
            // 
            this.text_box_perfmon_module_name.Location = new System.Drawing.Point(85, 57);
            this.text_box_perfmon_module_name.Name = "text_box_perfmon_module_name";
            this.text_box_perfmon_module_name.Size = new System.Drawing.Size(100, 20);
            this.text_box_perfmon_module_name.TabIndex = 3;
            // 
            // label_perfmon_module_name
            // 
            this.label_perfmon_module_name.AutoSize = true;
            this.label_perfmon_module_name.Location = new System.Drawing.Point(8, 60);
            this.label_perfmon_module_name.Name = "label_perfmon_module_name";
            this.label_perfmon_module_name.Size = new System.Drawing.Size(71, 13);
            this.label_perfmon_module_name.TabIndex = 2;
            this.label_perfmon_module_name.Text = "Module name";
            // 
            // text_box_perfmon_time
            // 
            this.text_box_perfmon_time.Location = new System.Drawing.Point(85, 21);
            this.text_box_perfmon_time.Name = "text_box_perfmon_time";
            this.text_box_perfmon_time.Size = new System.Drawing.Size(100, 20);
            this.text_box_perfmon_time.TabIndex = 1;
            // 
            // label_perfmon_time
            // 
            this.label_perfmon_time.AutoSize = true;
            this.label_perfmon_time.Location = new System.Drawing.Point(15, 25);
            this.label_perfmon_time.Name = "label_perfmon_time";
            this.label_perfmon_time.Size = new System.Drawing.Size(64, 13);
            this.label_perfmon_time.TabIndex = 0;
            this.label_perfmon_time.Text = "Monitor time";
            // 
            // Agent_window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 352);
            this.Controls.Add(this.panel_perfmon_configuration);
            this.Controls.Add(this.button_accept_agent_config);
            this.Controls.Add(this.panel_agent_configuration);
            this.Controls.Add(this.panel_zabbix_server);
            this.Controls.Add(this.menu);
            this.Name = "Agent_window";
            this.Text = "Agent configuration";
            this.group_box_zabbix_server.ResumeLayout(false);
            this.group_box_zabbix_server.PerformLayout();
            this.panel_zabbix_server.ResumeLayout(false);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.panel_agent_configuration.ResumeLayout(false);
            this.group_box_agent_config_timers.ResumeLayout(false);
            this.group_box_agent_config_timers.PerformLayout();
            this.group_box_agent_config_path.ResumeLayout(false);
            this.group_box_agent_config_path.PerformLayout();
            this.panel_perfmon_configuration.ResumeLayout(false);
            this.group_box_perfmon_config.ResumeLayout(false);
            this.group_box_perfmon_config.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox group_box_zabbix_server;
        private System.Windows.Forms.TextBox text_box_password;
        private System.Windows.Forms.Label label_password;
        private System.Windows.Forms.TextBox text_box_uid;
        private System.Windows.Forms.Label label_uid;
        private System.Windows.Forms.TextBox text_box_database_name;
        private System.Windows.Forms.Label label_database_name;
        private System.Windows.Forms.TextBox text_box_server_ip;
        private System.Windows.Forms.Label label_server_ip;
        private System.Windows.Forms.Panel panel_zabbix_server;
        private System.Windows.Forms.Panel panel_agent_configuration;
        private System.Windows.Forms.ToolStripMenuItem menu_zabbix_server;
        private System.Windows.Forms.ToolStripMenuItem menu_agent_configuration;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.Label label_log_path;
        private System.Windows.Forms.TextBox text_box_log_path;
        private System.Windows.Forms.Button button_accept_agent_config;
        private System.Windows.Forms.GroupBox group_box_agent_config_path;
        private System.Windows.Forms.GroupBox group_box_agent_config_timers;
        private System.Windows.Forms.TextBox text_box_process_timer;
        private System.Windows.Forms.Label label_process_timer;
        private System.Windows.Forms.Label label_data_timer;
        private System.Windows.Forms.TextBox text_box_data_timer;
        private System.Windows.Forms.TextBox text_box_analyzer_timer;
        private System.Windows.Forms.Label label_analyzer_timer;
        private System.Windows.Forms.TextBox text_box_sender_timer;
        private System.Windows.Forms.Label label_sender_timer;
        private System.Windows.Forms.ToolStripMenuItem menu_perfmon_configuration;
        private System.Windows.Forms.Panel panel_perfmon_configuration;
        private System.Windows.Forms.GroupBox group_box_perfmon_config;
        private System.Windows.Forms.TextBox text_box_perfmon_time;
        private System.Windows.Forms.Label label_perfmon_time;
        private System.Windows.Forms.TextBox text_box_perfmon_path;
        private System.Windows.Forms.Label label_perfmon_path;
        private System.Windows.Forms.TextBox text_box_perfmon_module_name;
        private System.Windows.Forms.Label label_perfmon_module_name;
    }
}