﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Windows.Forms;      
using System.Diagnostics;           
using AgentConfigurationSerializerLibrary;

namespace Agent_config
{
    public partial class Counters_window : Form
    {
        Configuration config;
        AgentSerializer agent_serializer;

        string connection_string;
        List<String> parameters_from_template;
        List<String> editable_parameters;
        Dictionary<String, String> dictionary_counter_name_category;

        string[] from_file_names;
        string[] from_file_categories;

        int host_id;
        int template_id;

        public Counters_window(string _connectionString, Configuration _config, AgentSerializer _agent_serializer)
        {
            InitializeComponent();
            connection_string = _connectionString;
            config = _config;
            agent_serializer = _agent_serializer;
            bool mysql_connection_ok = true;

            parameters_from_template = new List<String>();
            editable_parameters = new List<String>();
            dictionary_counter_name_category = new Dictionary<String, String>();

            try
            {
                test_mysql_connection();
            }
            catch (Exception ex)
            {
                mysql_connection_ok = false;
                button_accept_counter_config.Enabled = false;
                button_add_editable_parameters.Enabled = false;
                button_accept_editable_counter_config.Enabled = false;
                button_delete_editable_parameters.Enabled = false;
                string message = "Cannot connect to Zabbix server database.";
                MessageBox.Show(message);
            }

            if(mysql_connection_ok == true)
            {
                host_id = get_host_id();
                template_id = get_template_id();
                parameters_from_template = get_parameters_from_template(template_id);
                
                // List box template
                foreach (var param in parameters_from_template)
                    list_box_template_counters.Items.Add(param);

                from_file_names = System.IO.File.ReadAllLines("counter_list_names.txt");
                from_file_categories = System.IO.File.ReadAllLines("counter_list_categories.txt");

                for (int i = 0; i < from_file_names.Length; i++)
                    dictionary_counter_name_category.Add(from_file_names[i], from_file_categories[i]);

                // List box editable
                editable_parameters = get_editable_parameters();
                foreach (var param in editable_parameters)
                    list_box_editable_counters.Items.Add(param);

                // Check box list "Delete"
                foreach (var param in editable_parameters)
                    checked_list_editable_parameters_delete.Items.Add(param);

                // Combo box categories
                List<String> from_file_categories_unique = new List<String>();
                from_file_categories_unique.Add(from_file_categories[0]);
                for (int i = 1; i < from_file_categories.Length; i++)
                    if (from_file_categories[i] != from_file_categories[i - 1])
                        from_file_categories_unique.Add(from_file_categories[i]);

                for (int i = 0; i < from_file_categories_unique.Count; i++)
                    combo_box_add_editable_parameters.Items.Add(from_file_categories_unique[i]);
            }   
        }

        public void test_mysql_connection()
        {
            MySqlConnection db_test_connection = new MySqlConnection(connection_string);
            db_test_connection.Open();

            db_test_connection.Close();
        }

        private void button_accept_counter_config_Click(object sender, EventArgs e)
        {
            string list_box_selected_item = list_box_template_counters.GetItemText(list_box_template_counters.SelectedItem);
            string threshold_value = text_box_threshold.Text;
            string threshold_type = text_box_type_of_threshold.Text;
            string instance_name = combo_box_instance_name_template.SelectedItem.ToString();
            string alarm_counter = text_box_number_of_values_to_exceed_threshold.Text;

            if (!threshold_value.Equals("") && !threshold_type.Equals(""))
            {
                for(int i = 0; i < config.list_of_counters.Count; i++)
                {
                    if(config.list_of_counters[i].parameter == list_box_selected_item)
                    {
                        config.list_of_counters[i].threshold = Int32.Parse(threshold_value);
                        config.list_of_counters[i].type_of_threshold = threshold_type;
                        config.list_of_counters[i].instance_name = instance_name;
                        config.list_of_counters[i].number_of_values_to_exceed_threshold = Int32.Parse(alarm_counter);
                        break;
                    }
                }

                agent_serializer.write_to_config_file(config);

                string message = "Counter configuration changed.";
                MessageBox.Show(message);
            }
        }

        private int get_host_id()
        {
            int host_id = 0;
            string sql = "SELECT hostid FROM `hosts` WHERE name = '" + config.hostname + "'";

            try
            {
                using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                {
                    db_connection_mysql.Open();
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                host_id = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }
            catch
            {
                string message = "Error while getting host id.";
                MessageBox.Show(message);
            }

            return host_id;
        }

        private int get_template_id()
        {
            int template_id = 0;
            string sql = "SELECT templateid FROM `hosts_templates` WHERE hostid = " + host_id;

            try
            {
                using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                {
                    db_connection_mysql.Open();
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                template_id = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }
            catch
            {
                string message = "Error while getting template id.";
                MessageBox.Show(message);
            }

            return template_id;
        }

        private List<String> get_parameters_from_template(int _host_id)
        {
            List<String> parameters_list;
            parameters_list = new List<String>();
            string sql = "SELECT name FROM `items` WHERE hostid = " + _host_id;
            string parameter_name;

            try
            {
                using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                {
                    db_connection_mysql.Open();
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                parameter_name = reader.GetString(0);
                                parameters_list.Add(parameter_name);
                            }
                        }
                    }
                }
            }
            catch
            {
                string message = "Error while getting parameters from template.";
                MessageBox.Show(message);
            }

            return parameters_list;
        }

        private List<String> get_editable_parameters()
        {
            List<String> parameters_list;
            parameters_list = new List<String>();

            for (int i = 0; i < config.list_of_counters.Count; i++)
            {
                bool counter_from_config_file_is_on_counters_list = false;
                for (int j = 0; j < parameters_from_template.Count; j++)
                {
                    if (config.list_of_counters[i].parameter == parameters_from_template[j])
                    {
                        counter_from_config_file_is_on_counters_list = true;
                        break;
                    }
                }
                if (counter_from_config_file_is_on_counters_list == false)
                {
                    parameters_list.Add(config.list_of_counters[i].parameter);
                }
            }
            return parameters_list;
        }

        private void button_add_editable_parameters_Click(object sender, EventArgs e)
        {
            List<String> added_parameters_list = new List<String>();
            string temp = "";

            bool item_is_in_config_file_already = false;
            for (int i = 0; i < checked_list_editable_parameters_add.CheckedItems.Count; i++)
            {
                item_is_in_config_file_already = false;
                for (int j = 0; j < editable_parameters.Count; j++)
                    if(checked_list_editable_parameters_add.CheckedItems[i].ToString().Equals(editable_parameters[j]))
                        item_is_in_config_file_already = true;

                for (int j = 0; j < parameters_from_template.Count; j++)
                    if (checked_list_editable_parameters_add.CheckedItems[i].ToString().Equals(parameters_from_template[j]))
                        item_is_in_config_file_already = true;

                if(item_is_in_config_file_already == false)
                {
                    temp = checked_list_editable_parameters_add.CheckedItems[i].ToString();
                    added_parameters_list.Add(temp);
                }
            }

            for (int i = 0; i < added_parameters_list.Count; i++)
            {
                string instance;
                PerformanceCounterCategory temp_category = new PerformanceCounterCategory(dictionary_counter_name_category[added_parameters_list[i]]);
                // Category Type == 0 when category is of SingleInstance type
                if (temp_category.CategoryType == 0)
                    instance = "N";
                else
                    instance = "_Total";

                CounterConfiguration new_counter_config = new CounterConfiguration(dictionary_counter_name_category[added_parameters_list[i]], added_parameters_list[i], added_parameters_list[i], "-", 0, 1, instance);
                config.list_of_counters.Add(new_counter_config);
            }

            agent_serializer.write_to_config_file(config);

            string message = "Parameters added.";
            MessageBox.Show(message);
        }

        private void button_accept_editable_counter_config_Click(object sender, EventArgs e)
        {
            string list_box_selected_item = list_box_selected_item = list_box_editable_counters.GetItemText(list_box_editable_counters.SelectedItem);
            string threshold_value = threshold_value = text_box_threshold_editable.Text;
            string threshold_type = threshold_type = text_box_type_of_threshold_editable.Text;
            string instance_name = instance_name = combo_box_instance_name_editable.SelectedItem.ToString();
            string alarm_counter = text_box_number_of_values_to_exceed_threshold_editable.Text;

            if (!threshold_value.Equals("") && !threshold_type.Equals(""))
            {
                for (int i = 0; i < config.list_of_counters.Count; i++)
                {
                    if (config.list_of_counters[i].parameter == list_box_selected_item)
                    {
                        config.list_of_counters[i].threshold = Int32.Parse(threshold_value);
                        config.list_of_counters[i].type_of_threshold = threshold_type;
                        config.list_of_counters[i].instance_name = instance_name;
                        config.list_of_counters[i].number_of_values_to_exceed_threshold = Int32.Parse(alarm_counter);
                        break;
                    }
                }

                agent_serializer.write_to_config_file(config);
                
                string message = "Counter configuration changed.";
                MessageBox.Show(message);
            }
            else
            {
                string message = "Empty string values.";
                MessageBox.Show(message);
            }
        }

        private void button_delete_editable_parameters_Click(object sender, EventArgs e)
        {
            List<String> deleted_parameters_list = new List<String>();
            string temp = "";

            for (int i = 0; i < checked_list_editable_parameters_delete.CheckedItems.Count; i++)
            {
                temp = checked_list_editable_parameters_delete.CheckedItems[i].ToString();
                deleted_parameters_list.Add(temp);
            }

            for (int i = 0; i < deleted_parameters_list.Count; i++)
            {
                for (int j = 0; j < config.list_of_counters.Count; j++)
                    if (deleted_parameters_list[i] == config.list_of_counters[j].parameter)
                        config.list_of_counters.RemoveAt(j);
            }

            agent_serializer.write_to_config_file(config);

            string message = "Parameters deleted.";
            MessageBox.Show(message);
        }

        private void list_box_template_counters_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected_name = list_box_template_counters.SelectedItem.ToString();

            while (combo_box_instance_name_template.Items.Count > 0)
                combo_box_instance_name_template.Items.RemoveAt(0);
            PerformanceCounterCategory temp = new PerformanceCounterCategory(dictionary_counter_name_category[selected_name]);
            string[] instance_names = temp.GetInstanceNames();
            foreach (string name in instance_names)
                combo_box_instance_name_template.Items.Add(name);

            foreach (var counter_config in config.list_of_counters)
            {
                if(counter_config.parameter == selected_name)
                {
                    text_box_threshold.Text = counter_config.threshold.ToString();
                    text_box_type_of_threshold.Text = counter_config.type_of_threshold;
                    combo_box_instance_name_template.SelectedItem = counter_config.instance_name;
                    text_box_number_of_values_to_exceed_threshold.Text = counter_config.number_of_values_to_exceed_threshold.ToString();
                }
            }

            // Category Type == 0 when category is of SingleInstance type
            if (temp.CategoryType == 0)
            {
                combo_box_instance_name_template.Items.Add("N");
                combo_box_instance_name_template.SelectedItem = "N";
            }
        }

        private void list_box_editable_counters_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected_name = list_box_editable_counters.SelectedItem.ToString();

            while (combo_box_instance_name_editable.Items.Count > 0)
                combo_box_instance_name_editable.Items.RemoveAt(0);
            PerformanceCounterCategory temp_category = new PerformanceCounterCategory(dictionary_counter_name_category[selected_name]);
            string[] instance_names = temp_category.GetInstanceNames();
            foreach (string name in instance_names)
                combo_box_instance_name_editable.Items.Add(name);

            foreach (var counter_config in config.list_of_counters)
            {
                if (counter_config.parameter == selected_name)
                {
                    text_box_threshold_editable.Text = counter_config.threshold.ToString();
                    text_box_type_of_threshold_editable.Text = counter_config.type_of_threshold;
                    text_box_number_of_values_to_exceed_threshold_editable.Text = counter_config.number_of_values_to_exceed_threshold.ToString();
                    combo_box_instance_name_editable.SelectedItem = counter_config.instance_name;
                }
            }

            // Category Type == 0 when category is of SingleInstance type
            if (temp_category.CategoryType == 0)
            {
                combo_box_instance_name_editable.Items.Add("N");
                combo_box_instance_name_editable.SelectedItem = "N";
            }
        }

        private void combo_box_add_editable_parameters_SelectedIndexChanged(object sender, EventArgs e)
        {
            while (checked_list_editable_parameters_add.Items.Count > 0)
                checked_list_editable_parameters_add.Items.RemoveAt(0);

            for (int i = 0; i < from_file_names.Length; i++)
                if(dictionary_counter_name_category[from_file_names[i]].Equals(combo_box_add_editable_parameters.SelectedItem))
                    checked_list_editable_parameters_add.Items.Add(from_file_names[i]);

            for(int i = 0; i < editable_parameters.Count; i++)
                for (int j = 0; j < checked_list_editable_parameters_add.Items.Count; j++)
                    if (editable_parameters[i].Equals(checked_list_editable_parameters_add.Items[j]))
                        checked_list_editable_parameters_add.SetItemChecked(j, true);

            for (int i = 0; i < parameters_from_template.Count; i++)
                for (int j = 0; j < checked_list_editable_parameters_add.Items.Count; j++)
                    if (parameters_from_template[i].Equals(checked_list_editable_parameters_add.Items[j]))
                        checked_list_editable_parameters_add.SetItemChecked(j, true);
        }

    }
}
