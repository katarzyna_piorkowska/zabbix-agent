﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using AgentConfigurationSerializerLibrary;

namespace Agent_config
{
    public partial class Host_window : Form
    {
        string connection_string;
        Configuration config;
        AgentSerializer agent_serializer;

        public Host_window(string _connection_string, Configuration _config, AgentSerializer _agent_serializer)
        {
            InitializeComponent();
            connection_string = _connection_string;
            config = _config;
            agent_serializer = _agent_serializer;
            bool mysql_connection_ok = true;

            try
            {
                test_mysql_connection();
            }
            catch (Exception ex)
            {
                mysql_connection_ok = false;
                button_accept_host_config.Enabled = false;
                string message = "Cannot connect to Zabbix server database. " + ex.ToString();
                MessageBox.Show(message);
            }

            if(mysql_connection_ok == true)
            {
                List<String> templates_names = get_templates_names();
                for (int i = 0; i < templates_names.Count; i++)
                    combo_box_templates.Items.Add(templates_names[i]);
            }
        }

        public void test_mysql_connection()
        {
            MySqlConnection db_test_connection = new MySqlConnection(connection_string);
            db_test_connection.Open();
            db_test_connection.Close();
        }

        private void button_accept_host_config_Click(object sender, EventArgs e)
        {
            string combo_box_templates_selected_item = combo_box_templates.GetItemText(combo_box_templates.SelectedItem);
            string hostname = text_box_hostname.Text;

            if (!combo_box_templates_selected_item.Equals("") && !hostname.Equals(""))
            {
                config.hostname = hostname;
                config.template = combo_box_templates_selected_item;
                agent_serializer.write_to_config_file(config);

                string message = "Host configuration updated.";
                MessageBox.Show(message);
            }
            else
            {
                string message = "Empty string values.";
                MessageBox.Show(message);
            }
        }

        private List<String> get_templates_names()
        {
            string template_name;
            List<String> template_list;
            template_list = new List<String>();

            string sql = "SELECT host FROM `hosts` WHERE status=3";
            try
            {
                using (MySqlConnection db_connection_mysql = new MySqlConnection(connection_string))
                {
                    db_connection_mysql.Open();
                    using (MySqlCommand command = new MySqlCommand(sql, db_connection_mysql))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                template_name = reader.GetString(0);
                                template_list.Add(template_name);
                            }
                        }
                    }
                }
            }
            catch
            {
                string message = "Error while reading templates.";
                MessageBox.Show(message);
            }
            return template_list;
        }
    }
}
