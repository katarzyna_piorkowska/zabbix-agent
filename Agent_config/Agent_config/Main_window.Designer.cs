﻿namespace Agent_config
{
    partial class Main_window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_main_counters = new System.Windows.Forms.Button();
            this.button_main_agent = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_main_counters
            // 
            this.button_main_counters.Location = new System.Drawing.Point(59, 94);
            this.button_main_counters.Name = "button_main_counters";
            this.button_main_counters.Size = new System.Drawing.Size(144, 23);
            this.button_main_counters.TabIndex = 5;
            this.button_main_counters.Text = "Counters";
            this.button_main_counters.UseVisualStyleBackColor = true;
            this.button_main_counters.Click += new System.EventHandler(this.button_main_counters_Click);
            // 
            // button_main_agent
            // 
            this.button_main_agent.Location = new System.Drawing.Point(59, 150);
            this.button_main_agent.Name = "button_main_agent";
            this.button_main_agent.Size = new System.Drawing.Size(144, 23);
            this.button_main_agent.TabIndex = 6;
            this.button_main_agent.Text = "Agent configuration";
            this.button_main_agent.UseVisualStyleBackColor = true;
            this.button_main_agent.Click += new System.EventHandler(this.button_main_agent_Click);
            // 
            // button_main_host
            // 
            this.button1.Location = new System.Drawing.Point(59, 36);
            this.button1.Name = "button_main_host";
            this.button1.Size = new System.Drawing.Size(144, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Host information";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_main_host_Click);
            // 
            // Main_window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 194);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button_main_counters);
            this.Controls.Add(this.button_main_agent);
            this.Name = "Main_window";
            this.Text = "Zabbix Agent configuration";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_main_counters;
        private System.Windows.Forms.Button button_main_agent;
        private System.Windows.Forms.Button button1;
    }
}

