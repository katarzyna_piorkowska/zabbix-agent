﻿namespace Agent_config
{
    partial class Host_window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.text_box_hostname = new System.Windows.Forms.TextBox();
            this.label_hostname = new System.Windows.Forms.Label();
            this.label_template = new System.Windows.Forms.Label();
            this.combo_box_templates = new System.Windows.Forms.ComboBox();
            this.button_accept_host_config = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // text_box_hostname
            // 
            this.text_box_hostname.Location = new System.Drawing.Point(139, 35);
            this.text_box_hostname.Name = "text_box_hostname";
            this.text_box_hostname.Size = new System.Drawing.Size(196, 20);
            this.text_box_hostname.TabIndex = 3;
            // 
            // label_hostname
            // 
            this.label_hostname.AutoSize = true;
            this.label_hostname.Location = new System.Drawing.Point(46, 38);
            this.label_hostname.Name = "label_hostname";
            this.label_hostname.Size = new System.Drawing.Size(55, 13);
            this.label_hostname.TabIndex = 2;
            this.label_hostname.Text = "Hostname";
            // 
            // label_template
            // 
            this.label_template.AutoSize = true;
            this.label_template.Location = new System.Drawing.Point(46, 86);
            this.label_template.Name = "label_template";
            this.label_template.Size = new System.Drawing.Size(51, 13);
            this.label_template.TabIndex = 4;
            this.label_template.Text = "Template";
            // 
            // combo_box_templates
            // 
            this.combo_box_templates.FormattingEnabled = true;
            this.combo_box_templates.Location = new System.Drawing.Point(139, 83);
            this.combo_box_templates.Name = "combo_box_templates";
            this.combo_box_templates.Size = new System.Drawing.Size(196, 21);
            this.combo_box_templates.TabIndex = 5;
            // 
            // button_accept_host_config
            // 
            this.button_accept_host_config.Location = new System.Drawing.Point(49, 135);
            this.button_accept_host_config.Name = "button_accept_host_config";
            this.button_accept_host_config.Size = new System.Drawing.Size(286, 23);
            this.button_accept_host_config.TabIndex = 6;
            this.button_accept_host_config.Text = "OK";
            this.button_accept_host_config.UseVisualStyleBackColor = true;
            this.button_accept_host_config.Click += new System.EventHandler(this.button_accept_host_config_Click);
            // 
            // Host_window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 170);
            this.Controls.Add(this.button_accept_host_config);
            this.Controls.Add(this.combo_box_templates);
            this.Controls.Add(this.label_template);
            this.Controls.Add(this.text_box_hostname);
            this.Controls.Add(this.label_hostname);
            this.Name = "Host_window";
            this.Text = "Host_window";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_box_hostname;
        private System.Windows.Forms.Label label_hostname;
        private System.Windows.Forms.Label label_template;
        private System.Windows.Forms.ComboBox combo_box_templates;
        private System.Windows.Forms.Button button_accept_host_config;
    }
}