﻿using System;
using System.Diagnostics;           // for PerformanceCounter class
using System.Collections.Generic;   // for List class
using System.IO;                    // for File class
using System.Globalization;         // for CultureInfo


namespace Zabbix_agent
{
    class Counter
    {
        public PerformanceCounter counter;
        public Data data_unit;
        public string category;
        public string parameter;
        public string key;
        public string instance_name;
        public Analyzer_module analyzer;

        public Counter(string _category, string _parameter, string _key, int _id, string config = "N")
        {
            // _category - category of Performance Counter (Memory, Processor...)
            // _parameter - name of parameter
            // _id - parameter's id in Zabbix database
            // _threshold - if parameter's value is over (or under) threshold analyzer module collects detailed data (every 100ms)  
            // _type_of_threshold - indicator if value has to be > or < than threshold to be collected as detailed data
            // _config - optional parameter for some of Performance Counters constructors; instance name

            data_unit = new Data();
            category = _category;
            parameter = _parameter;
            key = _key;
            data_unit.name = _parameter;
            analyzer = new Analyzer_module(_category, _parameter, get_threshold_value(_key), get_threshold_type(_key));
            data_unit.id = _id;
            instance_name = config;

            try
            {
                if (config == "N")
                    counter = new PerformanceCounter(category, parameter);
                else
                    counter = new PerformanceCounter(category, parameter, config);
            }
            catch
            {
                Console.WriteLine("Data module: Problem while creating counter");
                lock (Agent.logger) { Agent.logger.log("Data module: Problem while creating counter"); }
            }
        }

        public char get_threshold_type(string _parameter_key)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(Agent.config_file_path);

            System.Xml.XmlNode node = doc.DocumentElement.SelectSingleNode("/config_file/counters/" + _parameter_key);
            string type = node.Attributes["type_of_threshold"].Value;

            return type[0];
        }

        public float get_threshold_value(string _parameter_key)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(Agent.config_file_path);
            float value = 0;

            System.Xml.XmlNode node = doc.DocumentElement.SelectSingleNode("/config_file/counters/" + _parameter_key);
            string text = node.Attributes["threshold"].Value;
            value = float.Parse(text, CultureInfo.InvariantCulture.NumberFormat);

            return value;
        }

        public String get_instance_name(string _parameter_key)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(Agent.config_file_path);
            String name = "";

            System.Xml.XmlNode node = doc.DocumentElement.SelectSingleNode("/config_file/counters/" + _parameter_key);
            name = node.Attributes["instance_name"].Value;

            return name;
        }

        public void update_counter_parameters(string _key)
        {
            float new_threshold_value = get_threshold_value(_key);
            char new_threshold_type = get_threshold_type(_key);
            string new_instance_name = get_instance_name(_key);
            analyzer.update_configuration(new_threshold_value, new_threshold_type);
            counter.InstanceName = new_instance_name;
        }
    }

    class Data_module
    {
        private List<Counter> list_of_counters;
        private Database_module database_module;
        private Perfmon_module perfmon_module;

        private int host_id;
        private int perfmon_trigger_id;

        public Data_module(int _host_id, string _connectionString)
        {
            list_of_counters = new List<Counter>();
            database_module = new Database_module(_connectionString);
            perfmon_module = new Perfmon_module();
            host_id = _host_id;
            perfmon_trigger_id = database_module.get_item_id("Trigger: Perfmon started", host_id);

            load_counters();
        }

        public void create_counter(string _category, string _name, string _key, string _config = "N")
        {
            int id = 0;
            id = database_module.get_item_id(_name, host_id);
            Counter c = new Counter(_category, _name, _key, id, _config);
            list_of_counters.Add(c);
        }

        public void load_counters()
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(Agent.config_file_path);
            System.Xml.XmlNode parent_node = doc.SelectSingleNode("config_file/counters");

            List<Counter> counters_from_config_file = new List<Counter>();
            foreach (System.Xml.XmlNode node in parent_node.ChildNodes)
            {
                Counter temp = new Counter(node.Attributes["category"].Value, node.Attributes["name"].Value, node.Name, database_module.get_item_id(node.Attributes["name"].Value, host_id), node.Attributes["instance_name"].Value);
                list_of_counters.Add(temp);
            }
        }

        public void get_data()
        {
            for (int i = 0; i < list_of_counters.Count; i++)
            {
                list_of_counters[i].data_unit.value = list_of_counters[i].counter.NextValue();
                list_of_counters[i].data_unit.clock = DateTime.Now;
            }

            for (int i = 0; i < list_of_counters.Count; i++)
                list_of_counters[i].analyzer.put_value_in_queue(list_of_counters[i].data_unit);
        }

        public void analyze_data()
        {
            Console.WriteLine("     Data module: Data analyzis started.");
            lock (Agent.logger) { Agent.logger.log("     Data module: Data analyzis started."); }
            List<Data> all_data_to_send = new List<Data>();
            List<Data> temp_list = new List<Data>();

            // Trigger starting perfmon monitoring
            bool perfmon_monitoring_trigger = false;

            for (int i = 0; i < list_of_counters.Count; i++)
            {
                list_of_counters[i].analyzer.get_value_from_queue();
                temp_list = list_of_counters[i].analyzer.analyze(ref perfmon_monitoring_trigger);
                all_data_to_send.AddRange(temp_list);
                list_of_counters[i].analyzer.data_to_analyze.Clear();

                if (perfmon_monitoring_trigger == true)
                {
                    perfmon_module.start_monitoring();

                    // Temp data to start Zabbix trigger
                    Data trigger_tag = new Data();
                    trigger_tag.id = perfmon_trigger_id;
                    trigger_tag.name = "Perfmon trigger";
                    trigger_tag.value = 10000;
                    trigger_tag.timestamp = 0;
                    trigger_tag.ns = 0;
                    all_data_to_send.Add(trigger_tag);
                }
            }

            database_module.insert_into_sqlite_database_in_bulk(all_data_to_send, all_data_to_send[0].name);

            Console.WriteLine("     Data module: Data analyzis finished. Records sent to local db = " + all_data_to_send.Count);
            lock (Agent.logger) { Agent.logger.log("     Data module: Data analyzis finished. Records sent to local db = " + all_data_to_send.Count); }
        }

        public void restart_data()
        {
            for (int i = 0; i < list_of_counters.Count; i++)
            {
                list_of_counters[i].analyzer.data_to_analyze.Clear();
                list_of_counters[i].analyzer.data_queue.Clear();
            }
        }

        public void update_configuration()
        {
            for (int i = 0; i < list_of_counters.Count; i++)
            {
                list_of_counters[i].update_counter_parameters(list_of_counters[i].key);
            }

            // Adding new counters from database to agent list
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(Agent.config_file_path);
            System.Xml.XmlNode parent_node = doc.SelectSingleNode("config_file/counters");

            List<Counter> counters_from_config_file = new List<Counter>();
            foreach (System.Xml.XmlNode node in parent_node.ChildNodes)
            {
                Counter temp = new Counter(node.Attributes["category"].Value, node.Attributes["name"].Value, node.Name, 0);
                counters_from_config_file.Add(temp);
            }

            for (int i = 0; i < counters_from_config_file.Count; i++)
            {
                bool counter_from_config_file_is_on_counters_list = false;
                for (int j = 0; j < list_of_counters.Count; j++)
                {
                    if (counters_from_config_file[i].parameter == list_of_counters[j].parameter)
                    {
                        counter_from_config_file_is_on_counters_list = true;
                        break;
                    }
                }
                if (counter_from_config_file_is_on_counters_list == false)
                {
                    // Check if counter is already in database
                    bool counter_is_in_db = database_module.check_if_counter_is_in_db(counters_from_config_file[i], host_id);
                    int new_item_id = 0;

                    if (counter_is_in_db == false)
                        new_item_id = database_module.add_new_counters_to_db(counters_from_config_file[i], host_id);
                    else
                        new_item_id = database_module.get_item_id(counters_from_config_file[i].parameter, host_id);

                    Counter temp = new Counter(counters_from_config_file[i].category, counters_from_config_file[i].parameter, counters_from_config_file[i].key, new_item_id);
                    list_of_counters.Add(temp);
                }
            }
        }
    }
}
