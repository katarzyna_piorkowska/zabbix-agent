using System;
using System.Threading; // for Thread class
using System.Timers;    // for Timer class
using System.IO;        // for FileSystemWatcher class
using System.Linq;      // for File.ReadAllLines
using System.Threading.Tasks;   // for Task class

namespace Zabbix_agent
{
    class Perfmon_module
    {
        bool perfmon_working;

        string module_name;
        string path_scripts;
        string path_reports;
        int perfmon_time;

        public Perfmon_module()
        {
            perfmon_working = false;

            perfmon_time = Int32.Parse(Agent.get_agent_config("/config_file/agent_config/perfmon", "time"));
            module_name = Agent.get_agent_config("/config_file/agent_config/perfmon", "module_name");
            path_scripts = Agent.get_agent_config("/config_file/agent_config/perfmon", "path_scripts");
            path_reports = Agent.get_agent_config("/config_file/agent_config/perfmon", "path_reports");
        }

        public void start_monitoring()
        {
            if (perfmon_working == false)
            {
                perfmon_working = true;
                Console.WriteLine("   Perfmon module: Perfmon monitoring started");
                lock (Agent.logger) { Agent.logger.log("   Perfmon module: Perfmon monitoring started"); }
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/C logman start " + module_name;
                startInfo.Verb = "runas";
                process.StartInfo = startInfo;
                process.Start();
                Task task = listen_to_changes_in_perfmon_files();
            }
        }

        private async Task listen_to_changes_in_perfmon_files()
        {
            System.Threading.Thread.Sleep(2000);
            await start_watcher();
            Console.WriteLine("   Perfmon module: Report file created");
            lock (Agent.logger) { Agent.logger.log("   Perfmon module: Report file created"); }
            System.Threading.Thread.Sleep(100);

            System.Diagnostics.Process proc = null;
            try
            {
                string targetDir = string.Format(path_scripts);
                proc = new System.Diagnostics.Process();
                proc.StartInfo.WorkingDirectory = targetDir;
                proc.StartInfo.FileName = "send_report_to_zabbix_server.bat";
                proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                proc.Start();
                proc.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine("   Perfmon module: Error in function send_report while sending report");
                lock (Agent.logger) { Agent.logger.log("   Perfmon module: Error in function send_report while sending report"); }
            }

            perfmon_working = false;
            Console.WriteLine("   Perfmon module: Report sent to Zabbix server");
            lock (Agent.logger) { Agent.logger.log("Perfmon module: Report sent to Zabbix server"); }
        }

        private Task start_watcher()
        {
            var tcs = new TaskCompletionSource<bool>();

            get_last_modified_directory();
            string path_to_report = path_reports + @"\" + module_name + @"\" + File.ReadLines(path_scripts + @"\" + "last_directory.txt").First() + @"\" + "report.html";

            FileSystemWatcher watcher = new FileSystemWatcher(Path.GetDirectoryName(path_to_report));
            FileSystemEventHandler createdHandler = null;

            createdHandler = (s, e) =>
            {
                if (e.Name == Path.GetFileName(path_to_report))
                {
                    tcs.TrySetResult(true);
                    watcher.Changed -= createdHandler;
                    watcher.Dispose();
                }
            };

            watcher.Changed += createdHandler;
            watcher.EnableRaisingEvents = true;

            return tcs.Task;
        }

        private void get_last_modified_directory()
        {
            System.Diagnostics.Process proc = null;
            try
            {
                string targetDir = string.Format(path_scripts);
                proc = new System.Diagnostics.Process();
                proc.StartInfo.WorkingDirectory = targetDir;
                proc.StartInfo.FileName = "get_last_modified_directory.bat";
                proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                proc.Start();
                proc.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Perfmon module: Error in function get_last_modified_directory");
                lock (Agent.logger) { Agent.logger.log("Perfmon module: Error in function get_last_modified_directory"); }
            }
        }
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      