﻿using System;
using System.Diagnostics;           // for Process class
using System.Collections.Generic;   // for List class

namespace Zabbix_agent
{
    struct _Process
    {
        public string name;
        public int id;
        public int timestamp_start;
        public int ns_start;
        public int timestamp_finish;
        public int ns_finish;
    }

    class Process_module
    {
        private static Database_module database_module;

        _Process temp_process;
        private Process[] table_of_processes;           // temporary storage for list of processes

        private List<_Process> list_of_processes;       // current system processes 
        private List<_Process> processes_to_send;       // list after analyzis; contains new processes that will be added to local db
        private List<_Process> current_db_processes;    // processes listed as current (not finished) in local database

        public Process_module(string _connectionString)
        {
            list_of_processes = new List<_Process>();
            processes_to_send = new List<_Process>();
            current_db_processes = new List<_Process>();
            _Process temp_process = new _Process();
            database_module = new Database_module(_connectionString);
        }

        public void get_process_list()
        {
            table_of_processes = Process.GetProcesses();
            DateTime time_now = DateTime.Now;
            int temp_timestamp = calculate_timestamp(time_now);
            int temp_ns = calculate_nanoseconds(time_now);
            foreach (Process single_process in table_of_processes)
            {
                temp_process.name = single_process.ProcessName;
                temp_process.id = single_process.Id;
                temp_process.timestamp_start = temp_timestamp;
                temp_process.ns_start = temp_ns;
                list_of_processes.Add(temp_process);
            }
        }

        public void analyze_list()
        {
            current_db_processes = database_module.select_current_processes_from_db();
            DateTime time_now = DateTime.Now;
            int temp_timestamp = calculate_timestamp(time_now);
            int temp_ns = calculate_nanoseconds(time_now);

            // This loop checks which unfinished processes from local db are finished during current iteration.
            for (int i = 0; i < current_db_processes.Count; i++)
            {
                bool process_is_ongoing = false;
                for (int j = 0; j < list_of_processes.Count; j++)
                {
                    if (current_db_processes[i].name == list_of_processes[j].name && current_db_processes[i].id == list_of_processes[j].id)
                    {
                        process_is_ongoing = true;
                        break;
                    }
                }
                if (process_is_ongoing == false)
                {
                    database_module.update_process_finish_time(current_db_processes[i], temp_timestamp, temp_ns);
                }
            }

            // This loop checks if the current process is in local db. If not, the process will be added.
            for (int i = 0; i < list_of_processes.Count; i++)
            {
                bool is_process_in_db = database_module.find_process_in_db(list_of_processes[i].id, list_of_processes[i].name);
                if (is_process_in_db == false)
                {
                    processes_to_send.Add(list_of_processes[i]);
                }
            }

            send_data_to_local_db();
        }

        public void send_data_to_local_db()
        {
            database_module.insert_into_sqlite_database_in_bulk(processes_to_send);
            list_of_processes.Clear();
            processes_to_send.Clear();
        }

        private int calculate_timestamp(DateTime _clock)
        {
            long timestamp = ((DateTimeOffset)_clock).ToUnixTimeSeconds();
            return unchecked((int)timestamp);
        }

        private int calculate_nanoseconds(DateTime _clock)
        {
            return _clock.Millisecond * 1000;
        }
    }
}
