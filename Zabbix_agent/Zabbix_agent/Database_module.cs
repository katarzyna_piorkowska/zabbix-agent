﻿using System;
using System.Data.SQLite;
using System.Collections.Generic;   // for List class  
using MySql.Data.MySqlClient;       // for MySQL connection
using System.Globalization;         // for NumberFormatInfo
using System.IO;                    // for File class

namespace Zabbix_agent
{
    class Database_module
    {
        SQLiteConnection db_connection;
        string sql;
        string connectionString;

        // Constructor for creating local db file
        public Database_module(string db_name, string _connectionString)
        {
            SQLiteConnection.CreateFile(db_name);
            db_connection = new SQLiteConnection("Data Source=LocalDB.sqlite;Version=3;");
            sql = "";
            connectionString = _connectionString;
            db_connection.Open();
        }

        // Constructor for operations on existing local db file
        public Database_module(string _connectionString)
        {
            db_connection = new SQLiteConnection("Data Source=LocalDB.sqlite;Version=3;");
            sql = "";
            connectionString = _connectionString;
            db_connection.Open();
        }

        // Creating databases data and processes
        public void create_database()
        {
            sql = "create table data (id int, item varchar(20), value float, timestamp int, ns int)";
            SQLiteCommand command = new SQLiteCommand(sql, db_connection);
            command.ExecuteNonQuery();
            sql = "create table processes (id int, name varchar(20), timestamp_start int, ns_start int, timestamp_finish int, ns_finish int)";
            command.CommandText = sql;
            command.ExecuteNonQuery();
        }

        // Finding host id in Mysql database having hostname
        public int get_host_id(string _hostname)
        {
            int host_id = 0;
            string sql = "SELECT hostid FROM `hosts` WHERE host='" + _hostname + "'";

            try
            {
                MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
                MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
                db_connection_mysql.Open();

                command_mysql.CommandText = sql;
                MySqlDataReader reader = command_mysql.ExecuteReader();


                while (reader.Read())
                {
                    host_id = reader.GetInt32(0);
                }

                db_connection_mysql.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function get_host_id");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function get_host_id"); }
            }
            return host_id;
        }

        // Finding parameter id having hostname and parameter name
        public int get_item_id(string _item_name, int _host_id)
        {
            int id = 0;
            string sql = "SELECT * FROM `items` WHERE hostid=" + _host_id + " AND name='" + _item_name + "'";
            
            try
            {
                MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
                MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
                db_connection_mysql.Open();
                

                command_mysql.CommandText = sql;
                MySqlDataReader reader = command_mysql.ExecuteReader();

                while (reader.Read())
                {
                    id = reader.GetInt32(0);
                }
                db_connection_mysql.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function get_item_id");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function get_item_id"); }
            }
            return id;
        }

        // Inserting new processes to local db  
        public void insert_into_sqlite_database_in_bulk(List<_Process> _list_of_processes)
        {
            using (var transaction = db_connection.BeginTransaction())
            {
                for (var i = 0; i < _list_of_processes.Count; i++)
                {
                    sql = "insert into processes (id, name, timestamp_start, ns_start, timestamp_finish, ns_finish) values (" + _list_of_processes[i].id + ", '" + _list_of_processes[i].name + "', " + _list_of_processes[i].timestamp_start + ", " + _list_of_processes[i].ns_start + ", 0, 0)";
                    SQLiteCommand command = new SQLiteCommand(sql, db_connection);
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
            }
        }

        // Inserting new data to local db  
        public void insert_into_sqlite_database_in_bulk(List<Data> _data_to_analyze, string _parameter)
        {
            using (var transaction = db_connection.BeginTransaction())
            {
                for (var i = 0; i < _data_to_analyze.Count; i++)
                {
                    sql = "insert into data (id, item, value, timestamp, ns) values (" + _data_to_analyze[i].id + ", '" + _data_to_analyze[i].name + "', '" + _data_to_analyze[i].value + "', " + _data_to_analyze[i].timestamp + ", " + _data_to_analyze[i].ns + ")";
                    SQLiteCommand command = new SQLiteCommand(sql, db_connection);
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
            }
        }

        // Checking if current process (_id, _name) is already in local db
        public bool find_process_in_db(int _id, string _name)
        {
            sql = "select * from processes where name = '" + _name + "' AND id = " + _id;
            SQLiteCommand command = new SQLiteCommand(sql, db_connection);
            SQLiteDataReader reader = command.ExecuteReader();
            int temp_timestamp = 1;
            while (reader.Read())
            {
                try
                {
                    temp_timestamp = reader.GetInt32(4);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Database module: Error in function find_process_in_db)");
                    lock (Agent.logger) { Agent.logger.log("Database module: Error in function find_process_in_db"); }
                }
            }

            // temp_timestamp equals zero only if the process is in db AND is unfinished; function returns true, so the process is not added again
            // temp_timestamp does not equal zero when there is no such process in db OR there is finished one; function returns false, so the process is added to db
            if (temp_timestamp == 0)
                return true;
            else
                return false;
        }

        // Selecting all unfinished processes from local db (timestamp_finish = 0)
        public List<_Process> select_current_processes_from_db()
        {
            List<_Process> processes_from_db = new List<_Process>();

            sql = "select * from processes where timestamp_finish = 0";
            SQLiteCommand command = new SQLiteCommand(sql, db_connection);
            SQLiteDataReader reader = command.ExecuteReader();
            _Process temp_process;
            temp_process.timestamp_finish = 0;
            temp_process.ns_finish = 0;

            while (reader.Read())
            {
                try
                {
                    temp_process.id = reader.GetInt32(0);
                    temp_process.name = reader.GetString(1);
                    temp_process.timestamp_start = reader.GetInt32(2);
                    temp_process.ns_start = reader.GetInt32(3);
                    processes_from_db.Add(temp_process);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Database module: Error in function select_current_processes_from_db");
                    lock (Agent.logger) { Agent.logger.log("Database module: Error in function select_current_processes_from_db"); }
                }
            }
            return processes_from_db;
        }

        // Setting process timestamp_finish to current time
        public void update_process_finish_time(_Process _process_to_update, int _timestamp, int _ns)
        {
            sql = "update processes set timestamp_finish = " + _timestamp + ", ns_finish = " + _ns + " where id = " + _process_to_update.id + " AND name = '" + _process_to_update.name + "' AND timestamp_start = " + _process_to_update.timestamp_start;
            try
            {
                SQLiteCommand command = new SQLiteCommand(sql, db_connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function update_process_finish_time");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function update_process_finish_time"); }
            }
        }

        // Selecting all data from local db to send
        public List<Data> select_data_from_sqlite_database(string _sql)
        {
            List<Data> data_from_db = new List<Data>();
            Data data_unit = new Data();

            SQLiteCommand command = new SQLiteCommand(_sql, db_connection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                try
                {
                    data_unit.id = reader.GetInt32(0);
                    double temp = reader.GetDouble(1);
                    data_unit.value = Convert.ToSingle(temp);
                    data_unit.timestamp = reader.GetInt32(2);
                    data_unit.ns = reader.GetInt32(3);
                    data_from_db.Add(data_unit);
                }
                catch
                {
                    data_unit.id = reader.GetInt32(0);
                    string temp = reader.GetString(1);
                    data_unit.value = float.Parse(temp);
                    data_unit.timestamp = reader.GetInt32(2);
                    data_unit.ns = reader.GetInt32(3);
                    data_from_db.Add(data_unit);
                }
            }
            return data_from_db;
        }

        // Selecting all processes from local db to send
        public List<_Process> select_processes_from_sqlite_database(string _sql)
        {
            List<_Process> processes_from_db = new List<_Process>();
            _Process temp_process = new _Process();

            SQLiteCommand command = new SQLiteCommand(_sql, db_connection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                try
                {
                    temp_process.id = reader.GetInt32(0);
                    temp_process.name = reader.GetString(1);
                    temp_process.timestamp_start = reader.GetInt32(2);
                    temp_process.ns_start = reader.GetInt32(3);
                    temp_process.timestamp_finish = reader.GetInt32(4);
                    temp_process.ns_finish = reader.GetInt32(5);
                    processes_from_db.Add(temp_process);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return processes_from_db;
        }

        // Deleting data from local db after send
        public void delete_data_from_local_db()
        {
            sql = "DELETE FROM data";
            SQLiteCommand command_delete = new SQLiteCommand(sql, db_connection);
            command_delete.ExecuteNonQuery();
        }

        // Deleting processes from local db after send
        public void delete_processes_from_local_db()
        {
            sql = "DELETE FROM processes where timestamp_finish != 0";
            SQLiteCommand command_delete = new SQLiteCommand(sql, db_connection);
            command_delete.ExecuteNonQuery();
        }

        // Sending data to Zabbix db
        public bool insert_into_mysql_database(List<Data> _data_from_local_db, List<_Process> _processes_from_local_db)
        {
            try
            {
                MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
                MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
                db_connection_mysql.Open();

                Console.WriteLine("Database module: Sending " + _data_from_local_db.Count + " data records, " + _processes_from_local_db.Count + " process records.");
                lock (Agent.logger) { Agent.logger.log("Database module: Sending " + _data_from_local_db.Count + " data records, " + _processes_from_local_db.Count + " process records."); }

                MySqlTransaction transaction;
                NumberFormatInfo nfi = new NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";

                using (transaction = db_connection_mysql.BeginTransaction())
                {
                    for (int i = 0; i < _data_from_local_db.Count; i++)
                    {
                        string value_as_string = _data_from_local_db[i].value.ToString(nfi);
                        sql = "INSERT INTO `history`(`itemid`, `clock`, `value`, `ns`) VALUES (" + _data_from_local_db[i].id + ", " + _data_from_local_db[i].timestamp + ", '" + value_as_string + "', " + _data_from_local_db[i].ns + ")";
                        command_mysql.CommandText = sql;
                        command_mysql.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }

                for (int i = 0; i < _processes_from_local_db.Count; i++)
                {
                    sql = "INSERT INTO `processes`(`id`, `name`, `timestamp_start`, `ns_start`, `timestamp_finish`, `ns_finish`) VALUES (" + _processes_from_local_db[i].id + ", '" + _processes_from_local_db[i].name + "', " + _processes_from_local_db[i].timestamp_start + ", " + _processes_from_local_db[i].ns_start + ", " + _processes_from_local_db[i].timestamp_finish + ", " + _processes_from_local_db[i].ns_finish + ")";
                    command_mysql.CommandText = sql;
                    command_mysql.ExecuteNonQuery();
                }

                db_connection_mysql.Close();
                Console.WriteLine("Database module: Data sent to MySQL database");
                lock (Agent.logger) { Agent.logger.log("Database module: Data sent to MySQL database"); }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function insert_into_mysql_database");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function insert_into_mysql_database"); }
                return false;
            }
        }

        public bool check_if_host_exists(string _hostname)
        {
            bool host_exists;
            int host_id = 0;

            MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
            MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
            db_connection_mysql.Open();

            sql = "SELECT hostid FROM `hosts` WHERE name='" + _hostname + "'";
            command_mysql.CommandText = sql;
            MySqlDataReader reader = command_mysql.ExecuteReader();
            while (reader.Read())
            {
                host_id = reader.GetInt32(0);
            }
            reader.Close();
            db_connection_mysql.Close();

            if (host_id == 0)
            {
                Console.WriteLine("Database module: Creating new host");
                lock (Agent.logger) { Agent.logger.log("Database module: Creating new host"); }
                host_exists = false;
            }
            else
            {
                Console.WriteLine("Database module: Host " + _hostname + " exists");
                lock (Agent.logger) { Agent.logger.log("Database module: Host " + _hostname + " exists"); }
                host_exists = true;
            }

            return host_exists;
        }

        public void register_host(string _hostname, string _template)
        {
            int template_id = get_host_template_id(_template);
  
            MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
            MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
                
            int next_host_id = get_next_host_id();
            int next_host_template_id = get_next_host_template_id();
            int next_item_id = get_next_item_id();

            db_connection_mysql.Open();

            try
            {
                sql = "INSERT INTO `hosts`(`hostid`, `proxy_hostid`, `host`, `status`, `disable_until`, `error`, `available`, `errors_from`, `lastaccess`, `ipmi_authtype`, `ipmi_privilege`, `ipmi_username`, `ipmi_password`, `ipmi_disable_until`, `ipmi_available`, `snmp_disable_until`, `snmp_available`, `maintenanceid`, `maintenance_status`, `maintenance_type`, `maintenance_from`, `ipmi_errors_from`, `snmp_errors_from`, `ipmi_error`, `snmp_error`, `jmx_disable_until`, `jmx_available`, `jmx_errors_from`, `jmx_error`, `name`, `flags`, `templateid`, `description`, `tls_connect`, `tls_accept`, `tls_issuer`, `tls_subject`, `tls_psk_identity`, `tls_psk`) VALUES (" + next_host_id + ",null,'" + _hostname + "',0,0,'',0,0,0,0,0,'','',0,0,0,0,null,0,0,0,0,0,'','',0,0,0,'','" + _hostname + "',0,null,'',0,0,'','','','')";
                command_mysql.CommandText = sql;
                command_mysql.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function register_host while adding host");
                Console.WriteLine(ex);
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function register_host while adding host"); }
            }
            try
            {
                sql = "INSERT INTO `hosts_templates`(`hosttemplateid`, `hostid`, `templateid`) VALUES (" + next_host_template_id + "," + next_host_id + "," + template_id + ")";
                command_mysql.CommandText = sql;
                command_mysql.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function register_host while adding host_template");
                Console.WriteLine(ex);
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function register_host while adding host_template"); }
            }

            // Here I select all items contained in chosen template
            List<String> names = new List<String>();
            List<String> keys = new List<String>();

            try
            {
                sql = "SELECT name,key_ FROM `items` WHERE hostid = " + template_id;
                command_mysql.CommandText = sql;
                MySqlDataReader reader = command_mysql.ExecuteReader();
                while (reader.Read())
                {
                    names.Add(reader.GetString(0));
                    keys.Add(reader.GetString(1));
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function register_host while selecting items");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function register_host while selecting items"); }
            }


            for(int i = 0; i < names.Count; i++)
            {
                add_item(command_mysql, next_item_id, next_host_id, names[i], keys[i]);
                next_item_id++;
            }

            try
            {
                next_host_id++;
                sql = "UPDATE `ids` SET `nextid`=" + next_host_id + " WHERE `field_name`='hostid'";
                command_mysql.CommandText = sql;
                command_mysql.ExecuteNonQuery();

                next_host_template_id++;
                sql = "UPDATE `ids` SET `nextid`=" + next_host_template_id + " WHERE `field_name`='hosttemplateid'";
                command_mysql.CommandText = sql;
                command_mysql.ExecuteNonQuery();

                sql = "UPDATE `ids` SET `nextid`=" + next_item_id + " WHERE `field_name`='itemid'";
                command_mysql.CommandText = sql;
                command_mysql.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function register_host while updating items");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function register_host while updating items"); }
            }
            finally
            {
                db_connection_mysql.Close();
            }
        }

        private int get_host_template_id(string _template)
        {
            int template_id = 0;
            string sql = "SELECT `hostid` FROM `hosts` WHERE name = '" + _template + "'";

            try
            {
                MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
                MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
                db_connection_mysql.Open();

                command_mysql.CommandText = sql;
                MySqlDataReader reader = command_mysql.ExecuteReader();

                while (reader.Read())
                {
                    template_id = reader.GetInt32(0);
                }
                db_connection_mysql.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function get_host_template_id");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function get_host_template_id"); }
            }
            return template_id;
        }

        private int get_next_host_id()
        {
            int next_host_id = 0;
            string sql = "SELECT `nextid` FROM `ids` WHERE field_name = 'hostid'";

            try
            {
                MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
                MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
                db_connection_mysql.Open();

                command_mysql.CommandText = sql;
                MySqlDataReader reader = command_mysql.ExecuteReader();

                while (reader.Read())
                {
                    next_host_id = reader.GetInt32(0);
                }

                db_connection_mysql.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function get_next_host_id");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function get_next_host_id"); }
            }
            return next_host_id;
        }

        private int get_next_host_template_id()
        {
            int next_host_template_id = 0;
            string sql = "SELECT `nextid` FROM `ids` WHERE field_name = 'hosttemplateid'";

            try
            {
                MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
                MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
                db_connection_mysql.Open();

                command_mysql.CommandText = sql;
                MySqlDataReader reader = command_mysql.ExecuteReader();


                while (reader.Read())
                {
                    next_host_template_id = reader.GetInt32(0);
                }

                db_connection_mysql.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function get_next_host_template_id");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function get_next_host_template_id"); }
            }
            return next_host_template_id;
        }

        private int get_next_item_id()
        {
            int next_item_id = 0;
            string sql = "SELECT `nextid` FROM `ids` WHERE field_name = 'itemid'";

            try
            {
                MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
                MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
                db_connection_mysql.Open();

                command_mysql.CommandText = sql;
                MySqlDataReader reader = command_mysql.ExecuteReader();


                while (reader.Read())
                {
                    next_item_id = reader.GetInt32(0);
                }

                db_connection_mysql.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function get_next_item_id");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function get_next_item_id"); }
            }
            return next_item_id;
        }

        private void add_item(MySqlCommand _command_mysql, int _next_item_id, int _next_host_id, string item_name, string item_key)
        {
            try
            {
                // New version of database
                //sql = "INSERT INTO `items`(`itemid`, `type`, `snmp_community`, `snmp_oid`, `hostid`, `name`, `key_`, `delay`, `history`, `trends`, `status`, `value_type`, `trapper_hosts`, `units`, `multiplier`, `delta`, `snmpv3_securityname`, `snmpv3_securitylevel`, `snmpv3_authpassphrase`, `snmpv3_privpassphrase`, `formula`, `error`, `lastlogsize`, `logtimefmt`, `templateid`, `valuemapid`, `delay_flex`, `params`, `ipmi_sensor`, `data_type`, `authtype`, `username`, `password`, `publickey`, `privatekey`, `mtime`, `flags`, `interfaceid`, `port`, `description`, `inventory_link`, `lifetime`, `snmpv3_authprotocol`, `snmpv3_privprotocol`, `state`, `snmpv3_contextname`, `evaltype`) VALUES(" + _next_item_id + ", 2, '', '', " + _next_host_id + ", '" + item_name + "', '" + item_key + "', '1m', '365d', '354d', 0, 3, '', '', 0, 0, '', 0, '', '', '', '', 0, '', null, null, '', '', '', 0, 0, '', '', '', '', 0, 0, null, '', '', 0, '', 0, 0, 0, '', 0)";
                // My computer
                sql = "INSERT INTO `items`(`itemid`, `type`, `snmp_community`, `snmp_oid`, `hostid`, `name`, `key_`, `delay`, `history`, `trends`, `status`, `value_type`, `trapper_hosts`, `units`, `snmpv3_securityname`, `snmpv3_securitylevel`, `snmpv3_authpassphrase`, `snmpv3_privpassphrase`, `formula`, `error`, `lastlogsize`, `logtimefmt`, `templateid`, `valuemapid`, `params`, `ipmi_sensor`, `authtype`, `username`, `password`, `publickey`, `privatekey`, `mtime`, `flags`, `interfaceid`, `port`, `description`, `inventory_link`, `lifetime`, `snmpv3_authprotocol`, `snmpv3_privprotocol`, `state`, `snmpv3_contextname`, `evaltype`, `jmx_endpoint`, `master_itemid`) VALUES(" + _next_item_id + ", 2, '', '', " + _next_host_id + ", '" + item_name + "', '" + item_key + "', '1m', '1w', '354d', 0, 3, '', '', '', 0, '', '', '', '', 0, '', null, null, '', '', 0, '', '', '', '', 0, 0, null, '', '', 0, '', 0, 0, 0, '', 0, '', null)";
                _command_mysql.CommandText = sql;
                _command_mysql.ExecuteNonQuery();

                Console.WriteLine("Database module: New item added");
                lock (Agent.logger) { Agent.logger.log("Database module: New item added"); }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function add_item");
                Console.WriteLine(ex);
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function add_item"); }
                lock (Agent.logger) { Agent.logger.log(ex.ToString()); }
            }
        }

        public int add_new_counters_to_db(Counter _counter_from_config_file, int _host_id)
        {

            int next_item_id = get_next_item_id();

            try
            {
                MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
                MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
                db_connection_mysql.Open();

                add_item(command_mysql, next_item_id, _host_id, _counter_from_config_file.parameter, _counter_from_config_file.key);
                next_item_id++;

                sql = "UPDATE `ids` SET `nextid`=" + next_item_id + " WHERE `field_name`='itemid'";
                command_mysql.CommandText = sql;
                command_mysql.ExecuteNonQuery();

                db_connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function add_new_counters_to_db");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function add_new_counters_to_db"); }
            }

            return next_item_id;
        }

        public bool check_if_counter_is_in_db(Counter _counter_from_config_file, int _host_id)
        {
            bool already_in_db = false;

            try
            {
                MySqlConnection db_connection_mysql = new MySqlConnection(connectionString);
                MySqlCommand command_mysql = new MySqlCommand(sql, db_connection_mysql);
                db_connection_mysql.Open();

                sql = "SELECT itemid FROM `items` WHERE hostid = " + _host_id + " AND name = '" + _counter_from_config_file.parameter + "'";
                command_mysql.CommandText = sql;
                MySqlDataReader reader = command_mysql.ExecuteReader();

                while (reader.Read())
                {
                    already_in_db = true;
                }

                db_connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database module: Error in function check_if_counter_is_in_db");
                lock (Agent.logger) { Agent.logger.log("Database module: Error in function check_if_counter_is_in_db"); }
            }

            return already_in_db;
        }
    }
}
