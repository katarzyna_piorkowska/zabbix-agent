﻿using System;
using System.Threading;         // for Thread class
using System.Timers;            // for Timer class
using System.IO;                // for File class
using System.Threading.Tasks;   // for Task class

namespace Zabbix_agent
{
    class Logger
    {
        public string log_path;

        public Logger()
        { }

        public void log(string log_message)
        {
            log_message = log_message + Environment.NewLine;
            File.AppendAllText(log_path, log_message);
        }
    }

    class Agent
    {
        public static string config_file_path;

        private static Data_module data_module;
        private static Process_module process_module;
        private static Sender_module sender_module;
        private static Database_module database_module;
        public static Logger logger;

        private static System.Timers.Timer data_module_timer;
        private static System.Timers.Timer process_module_timer;
        private static System.Timers.Timer analyzer_module_timer;
        private static System.Timers.Timer sender_module_timer;
        private static int data_module_timer_interval;
        private static int process_module_timer_interval;
        private static int analyzer_module_timer_interval;
        private static int sender_module_timer_interval;

        private static Thread t1;
        private static Thread t2;
        private static Thread t3;
        private static Thread t4;

        private static bool analyzer_reboot;
        private static int unfinished_threads_counter;
        private static string hostname;
        private static string template;
        private static int host_id;
        private static string server;
        private static string database;
        private static string uid;
        private static string password;
        public static int analyzers_queue_size;

        static bool config_file_changed;

        static void Main(string[] args)
        {
            //config_file_path = System.AppDomain.CurrentDomain.BaseDirectory + @"Agent_config\config_file.xml";
            config_file_path = @"B:\Mgr\Zabbix_agent\config_file.xml";
            logger = new Logger();
            config_file_changed = false;
            load_config_file();
            configuration_module_handler();
            string connectionString = "SERVER=" + server + "; PORT = 3306 ;" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            bool agent_start_ok = true;
            try
            {
                database_module = new Database_module("LocalDB.sqlite", connectionString);
                database_module.create_database();
            }
            catch
            {
                Console.WriteLine("Problem with creating local db. Restarting agent...");
                lock (logger) { logger.log("Problem with creating local db. Restarting agent..."); }
                agent_start_ok = false;
            }

            if (agent_start_ok == true)
            {
                if (check_if_host_exists(hostname) == false)
                    register_host_in_db(hostname, template);

                host_id = database_module.get_host_id(hostname);
                data_module = new Data_module(host_id, connectionString);
                sender_module = new Sender_module(server, hostname, connectionString);
                process_module = new Process_module(connectionString);
                analyzer_reboot = false;
                unfinished_threads_counter = 0;

                t1 = new Thread(data_module_handler);
                t2 = new Thread(process_module_handler);
                t3 = new Thread(analyzer_module_handler);
                t4 = new Thread(sender_module_handler);

                Console.WriteLine("Starting data module");
                lock (logger) { logger.log("Starting data module"); }
                t1.Start();

                Console.WriteLine("Starting process module");
                lock (logger) { logger.log("Starting process module"); }
                t2.Start();

                Console.WriteLine("Starting analyzer module");
                lock (logger) { logger.log("Starting analyzer module"); }
                t3.Start();

                System.Threading.Thread.Sleep(5000);
                Console.WriteLine("Starting sender module");
                lock (logger) { logger.log("Starting sender module"); }
                t4.Start();

                while (1 == 1)
                {
                    if (analyzer_reboot == true)
                    {
                        Console.WriteLine("Restarting agent...");
                        lock (logger) { logger.log("Restarting agent..."); }
                        System.Diagnostics.Process.Start(System.AppDomain.CurrentDomain.FriendlyName);
                        Environment.Exit(0);
                    }

                    if (config_file_changed == true)
                    {
                        config_file_changed = false;
                        configuration_module_handler();
                    }
                    System.Threading.Thread.Sleep(2000);
                }
            }
            else
            {
                System.Threading.Thread.Sleep(5000);
                System.Diagnostics.Process.Start(System.AppDomain.CurrentDomain.FriendlyName);
                Environment.Exit(0);
            }
        }

        private static void data_module_handler()
        {
            data_module_timer = new System.Timers.Timer();
            data_module_timer.Elapsed += new ElapsedEventHandler(on_data_module_timer_event);
            data_module_timer.Interval = data_module_timer_interval;
            data_module_timer.Enabled = true;
        }

        private static void process_module_handler()
        {
            process_module_timer = new System.Timers.Timer();
            process_module_timer.Elapsed += new ElapsedEventHandler(on_process_module_timer_event);
            process_module_timer.Interval = process_module_timer_interval;
            process_module_timer.Enabled = true;
        }

        private static void analyzer_module_handler()
        {
            analyzer_module_timer = new System.Timers.Timer();
            analyzer_module_timer.Elapsed += new ElapsedEventHandler(on_analyzer_module_timer_event);
            analyzer_module_timer.Interval = analyzer_module_timer_interval;
            analyzer_module_timer.Enabled = true;
        }

        private static void sender_module_handler()
        {
            sender_module_timer = new System.Timers.Timer();
            sender_module_timer.Elapsed += new ElapsedEventHandler(on_sender_module_timer_event);
            sender_module_timer.Interval = sender_module_timer_interval;
            sender_module_timer.Enabled = true;
        }

        private static void configuration_module_handler()
        {
            Task task = listen_to_changes_in_config_file();
        }

        private static void on_data_module_timer_event(Object source, System.Timers.ElapsedEventArgs e)
        {
            data_module.get_data();
        }

        private static void on_process_module_timer_event(Object source, System.Timers.ElapsedEventArgs e)
        {
            process_module.get_process_list();
            process_module.analyze_list();
        }

        private static void on_analyzer_module_timer_event(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (unfinished_threads_counter > 4)
                analyzer_reboot = true;
            unfinished_threads_counter += 1;
            data_module.analyze_data();
            unfinished_threads_counter = 0;
        }

        private static void on_sender_module_timer_event(Object source, System.Timers.ElapsedEventArgs e)
        {
            sender_module.send_data_to_zabbix_db();
        }

        private static void load_config_file()
        {
            // Server configuration
            server = get_agent_config("/config_file/agent_config/server", "ip");
            database = get_agent_config("/config_file/agent_config/server", "database");
            uid = get_agent_config("/config_file/agent_config/server", "uid");
            password = get_agent_config("/config_file/agent_config/server", "password");

            // Host configuration
            hostname = get_agent_config("/config_file/host_config/host", "name");
            template = get_agent_config("/config_file/host_config/host", "template");

            // Paths
            logger.log_path = get_agent_config("/config_file/agent_config/path", "log");

            // Timer intervals
            data_module_timer_interval = Int32.Parse(get_agent_config("/config_file/agent_config/timers", "data_module"));
            process_module_timer_interval = Int32.Parse(get_agent_config("/config_file/agent_config/timers", "process_module"));
            analyzer_module_timer_interval = Int32.Parse(get_agent_config("/config_file/agent_config/timers", "analyzer_module"));
            sender_module_timer_interval = Int32.Parse(get_agent_config("/config_file/agent_config/timers", "sender_module"));

            if (data_module_timer != null)
            {
                data_module_timer.Stop();
                data_module_timer.Interval = data_module_timer_interval;
                data_module_timer.Start();
            }
            if (process_module_timer != null)
            { 
                process_module_timer.Stop();
                process_module_timer.Interval = process_module_timer_interval;
                process_module_timer.Start();
            }
            if (analyzer_module_timer != null)
            {
                analyzer_module_timer.Stop();
                analyzer_module_timer.Interval = analyzer_module_timer_interval;
                analyzer_module_timer.Start();
            }
            if (sender_module_timer != null)
            {
                sender_module_timer.Stop();
                sender_module_timer.Interval = sender_module_timer_interval;
                sender_module_timer.Start();
            }
            analyzers_queue_size = analyzer_module_timer_interval / data_module_timer_interval;
        }

        public static string get_agent_config(string _node_name, string _attribute)
        {

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(config_file_path);

            System.Xml.XmlNode node = doc.DocumentElement.SelectSingleNode(_node_name);
            string text = node.Attributes[_attribute].Value;
            return text;
        }

        private static async Task listen_to_changes_in_config_file()
        {
            await start_watcher();
            Console.WriteLine("Configuration file changed");
            lock (logger) { logger.log("Configuration file changed"); }
            System.Threading.Thread.Sleep(100);
            load_config_file();
            data_module.update_configuration();
            config_file_changed = true;

            System.Diagnostics.Process.Start(System.AppDomain.CurrentDomain.FriendlyName);
            Environment.Exit(0);
        }

        private static Task start_watcher()
        {
            var tcs = new TaskCompletionSource<bool>();

            FileSystemWatcher watcher = new FileSystemWatcher(Path.GetDirectoryName(config_file_path));
            FileSystemEventHandler changedHandler = null;

            changedHandler = (s, e) =>
            {
                if (e.Name == Path.GetFileName(config_file_path))
                {
                    tcs.TrySetResult(true);
                    watcher.Changed -= changedHandler;
                    watcher.Dispose();
                }
            };

            watcher.Changed += changedHandler;
            watcher.EnableRaisingEvents = true;

            return tcs.Task;
        }

        private static bool check_if_host_exists(string _hostname)
        {
            bool check = database_module.check_if_host_exists( _hostname);
            return check;
        }

        private static void register_host_in_db(string _hostname, string _template)
        {
            database_module.register_host(_hostname, _template);
        }
    }
}

