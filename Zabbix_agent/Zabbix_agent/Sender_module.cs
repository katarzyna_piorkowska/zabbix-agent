﻿using System.Collections.Generic;   // for List class
using System.Web.Script.Serialization;
using System;
using System.Net.Sockets;
using System.Text;


namespace Zabbix_agent
{
    class Sender_module
    {
        Database_module database_module;
        List<Data> data_from_local_db;
        List<_Process> processes_from_local_db;
        string server_ip;
        int server_port = 10051;
        string hostname;
        bool zabbix_trigger;

        string connectionString;

        public Sender_module(string _server, string _hostname, string _connectionString)
        {
            data_from_local_db = new List<Data>();
            processes_from_local_db = new List<_Process>();
            server_ip = _server;
            hostname = _hostname;
            connectionString = _connectionString;
            zabbix_trigger = false;
        }

        public void get_data_from_local_db()
        {
            database_module = new Database_module(connectionString);
            string sql = "SELECT id, value, timestamp, ns FROM data";
            data_from_local_db = database_module.select_data_from_sqlite_database(sql);

            sql = "SELECT * FROM processes where timestamp_finish != 0";
            processes_from_local_db = database_module.select_processes_from_sqlite_database(sql);

            // Zabbix triggers
            int index = 0;
            for (int i = data_from_local_db.Count - 1; i >= 0; i--)
            {
                if(data_from_local_db[i].timestamp == 0)
                {
                    zabbix_trigger = true;
                    index = i;
                    break;
                }
            }
            if (zabbix_trigger == true)
                data_from_local_db.RemoveAt(index);
        }

        public void send_data_to_zabbix_db()
        {
            get_data_from_local_db();
            bool send_status = database_module.insert_into_mysql_database(data_from_local_db, processes_from_local_db);

            if(send_status == true)
            {
                database_module.delete_data_from_local_db();
                database_module.delete_processes_from_local_db();
            }

            // Zabbix triggers
            if(zabbix_trigger == true)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string json = serializer.Serialize(
                    new
                    {
                        request = "sender data",
                        data = new[]
                        {
            new
            {
                host = hostname,
                key = "trigger.perfmon",
                value = 1
            }
                        }
                    });


                byte[] header = Encoding.ASCII.GetBytes("ZBXD\x01");
                byte[] length = BitConverter.GetBytes((long)json.Length);
                byte[] data = Encoding.ASCII.GetBytes(json);

                byte[] all = new byte[header.Length + length.Length + data.Length];

                System.Buffer.BlockCopy(header, 0, all, 0, header.Length);
                System.Buffer.BlockCopy(length, 0, all, header.Length, length.Length);
                System.Buffer.BlockCopy(data, 0, all, header.Length + length.Length,
                    data.Length);

                using (var client = new Socket(AddressFamily.InterNetwork, SocketType.Stream,
                    ProtocolType.Tcp))
                {
                    client.Connect(server_ip, server_port);
                    client.Send(all);
                }
            }
            if (zabbix_trigger == false)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string json = serializer.Serialize(
                    new
                    {
                        request = "sender data",
                        data = new[]
                        {
            new
            {
                host = hostname,
                key = "trigger.perfmon",
                value = 0
            }
                        }
                    });


                byte[] header = Encoding.ASCII.GetBytes("ZBXD\x01");
                byte[] length = BitConverter.GetBytes((long)json.Length);
                byte[] data = Encoding.ASCII.GetBytes(json);

                byte[] all = new byte[header.Length + length.Length + data.Length];

                System.Buffer.BlockCopy(header, 0, all, 0, header.Length);
                System.Buffer.BlockCopy(length, 0, all, header.Length, length.Length);
                System.Buffer.BlockCopy(data, 0, all, header.Length + length.Length,
                    data.Length);

                using (var client = new Socket(AddressFamily.InterNetwork, SocketType.Stream,
                    ProtocolType.Tcp))
                {
                    client.Connect(server_ip, server_port);
                    client.Send(all);
                }
            }
        }
    }
}
