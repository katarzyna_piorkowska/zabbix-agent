﻿using System;
using System.Collections.Generic;   // for List and Queue classes

namespace Zabbix_agent
{
    struct Data
    {
        public int id;
        public string name;
        public float value;
        public int timestamp;
        public int ns;
        public DateTime clock;
    }

    class Analyzer_module
    {
        public List<Data> data_to_analyze;
        public Queue<Data> data_queue;
        float threshold;
        char type_of_threshold;
        string parameter;
        int size_of_queue;

        public Analyzer_module(string _category, string _parameter, float _threshold, char _type_of_threshold)
        {
            data_queue = new Queue<Data>();
            data_to_analyze = new List<Data>();
            threshold = _threshold;
            type_of_threshold = _type_of_threshold;
            parameter = _parameter;
            size_of_queue = Agent.analyzers_queue_size;
        }

        public void put_value_in_queue(Data data_unit)
        {
            data_unit.timestamp = calculate_timestamp(data_unit.clock);
            data_unit.ns = calculate_nanoseconds(data_unit.clock);
            data_queue.Enqueue(data_unit);
        }

        public void get_value_from_queue()
        {
            if (data_queue.Count >= size_of_queue)
            {
                for (int i = 0; i < size_of_queue; i++)
                    data_to_analyze.Add(data_queue.Dequeue());
            }
        }

        public List<Data> analyze(ref bool perfmon_monitoring_trigger)
        {
            bool counter_generates_trigger = false;
            float sum = 0;
            List<Data> data_to_send = new List<Data>();
            if (data_to_analyze.Count >= size_of_queue)
            {
                for (int i = 0; i < size_of_queue; i++)
                {
                    if (type_of_threshold == '>')
                    {
                        if (data_to_analyze[i].value > threshold)
                        {
                            counter_generates_trigger = true;
                            break;
                        }
                    }
                    if (type_of_threshold == '<')
                    {
                        if (data_to_analyze[i].value < threshold)
                        {
                            counter_generates_trigger = true;
                            break;
                        }
                    }
                    sum += data_to_analyze[i].value;
                }
                float avg = sum / size_of_queue;

                if (counter_generates_trigger == false)
                {
                    data_to_send.Clear();
                    Data data_unit = new Data();
                    data_unit.name = data_to_analyze[0].name;
                    data_unit.id = data_to_analyze[0].id;
                    data_unit.value = avg;
                    data_unit.timestamp = data_to_analyze[0].timestamp;
                    data_unit.ns = data_to_analyze[0].ns;
                    data_to_send.Add(data_unit);
                    return (data_to_send);
                }
                else
                {
                    perfmon_monitoring_trigger = true;
                    return (data_to_analyze);
                }
            }
            return data_to_send;
        }

        private int calculate_timestamp(DateTime _clock)
        {
            long timestamp = ((DateTimeOffset)_clock).ToUnixTimeSeconds();
            return unchecked((int)timestamp);
        }

        private int calculate_nanoseconds(DateTime _clock)
        {
            return _clock.Millisecond * 1000;
        }

        public void update_configuration(float _new_threshold_value, char _new_threshold_type)
        {
            threshold = _new_threshold_value;
            type_of_threshold = _new_threshold_type;
        }
    }
}

