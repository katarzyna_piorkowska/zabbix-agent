#!/bin/bash

while [ 1 ]
do
	today=`date '+%Y_%m_%d__%H_%M_%S'`;
        if [ -e /storage/report.html ]
        then
                mv /storage/report.html /storage/$today.report.html
        fi

        sleep 10
done
